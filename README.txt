ELE778 Repository

Introduction

Training a Network
	1) In order to Train a Network, you must first modify the parameters in the 'NeuralNetwork.config'.
	2) Here is an example of the minimal amout of modifcation that needs to be done in this config File:
		a)	'SizeOfNeuralNetworkChosen' 			//Chose the Network Size (Number of layers, 5 Layers are allowed maximum)
		b)  'NumberOfDataKeptPerSample' 			//Determine the number of elements taken per sample
		c)	'NumberOfNeuronsForFirstLayer'		//Allocate number of neurons to the first layer, all other layers
												//will resize according to a decreasing average
		d)	Any other parameters can be changed as wanted. The details are
			explained in the config file.	
	3) Run the executable and select option 'n'. 
	4) The training will hault upon reaching a given success rate set from the parameter 'SuccessRateThreshold'.
	5) An Output file will be generated during this process. A bin file containing the Weights and other properties of the NN
	   will be also generated.

Reloading a Network
	1) Modify the parameter 'LoadWeightsStructureFromPath' in the config file (without the file extension) and save it.
	2) Rune the executable and chose option 'y'.
	3) The program will now resum where it had left off. 




Associated File for ELE778 Lab2
	file_access.cpp
	file_access.h
	NeuralNet.cpp
	NeuralNet.h
	Object_Creator.cpp (main)