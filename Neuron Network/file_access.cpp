/*******************************************************************/
/************************Libraries Included*************************/
/*******************************************************************/
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <windows.h>
#include <iostream>
#include <tchar.h>
#include "file_access.h"
#include "dirent.h"
#include <algorithm>



using namespace std;

string my_current_path()
{
	TCHAR FilePath[MAX_PATH];
	if (GetCurrentDirectory(MAX_PATH, FilePath))
	{
		wstring wstring_FilePath(&FilePath[0]);
		string string_FilePath(wstring_FilePath.begin(), wstring_FilePath.end());
		return string_FilePath;
	}
	else
		return "";
}

int read_file(File_Class& parsing_file)
{
	
	/*Local Variable Declarations*/
	string current_line;
	ifstream myfile(parsing_file.filepath);
	vector<string> rawdata;									    //1 Dimensional Array of a long string per line

	string token;												//StringStream Variables
	stringstream ss("");										//Create empty stringstream.
	int j;


	if (myfile.is_open())
	{
		while (getline(myfile, current_line))					//Fetch every line until EOF
			rawdata.push_back(current_line);

		myfile.close();											//Close Opened File
	}
	else
	{
																//Upon error, the function will return an array of 0x0
		cout << "Your File Doesn't Exist..." << endl;
		return 0;
	}


	//Resizing array to fit number of lines according to Element type
	if(parsing_file.element_type == constants::ELEMENT_TYPE::FLOAT_TYPE)		//Fetch floating type data from text file
	{
		parsing_file.float_parsed_rawdata.resize(rawdata.size(), vector<float>(parsing_file.number_of_elements_line, constants::INIT_DATA_VALUES));
		//std::fill(parsing_file.float_parsed_rawdata.begin(), parsing_file.float_parsed_rawdata.end(), constants::INIT_DATA_VALUES);

		for (std::vector<int>::size_type i = 0; i != rawdata.size(); i++)
		{															//Parse every line and transfer to 2D Array						
																	//Reseting StringStream Local vars
			ss.clear();
			ss.str(rawdata[i]);

			for (j = 0; j < parsing_file.number_of_elements_line; )
			{														
				getline(ss, token, parsing_file.delimiter);
																				//Fetch every float in line and trf to array with token
				parsing_file.float_parsed_rawdata[i][j] = stof(token, NULL);	//Transfer Current Index
				j++;
			}
		}

		return 1;													//At this point we should have all Data in our 2D Vector
	}
	else if (parsing_file.element_type == constants::ELEMENT_TYPE::STRING_TYPE)	//Fetch String type data from text file
	{
		parsing_file.string_parsed_rawdata.resize(rawdata.size(), vector<std::string>(parsing_file.number_of_elements_line));

		for (int i = 0; i != rawdata.size(); i++)
		{															//Parse every line and transfer to 2D Array						
																	//Reseting StringStream Local vars
			ss.clear();
			ss.str(rawdata[i]);

			for (j = 0; j < parsing_file.number_of_elements_line; )
			{	
				getline(ss, token, parsing_file.delimiter);
				parsing_file.string_parsed_rawdata[i][j] = token;	//Transfer Current Index
				j++;
			}
		}

		return 1;													//At this point we should have all Data in our 2D Vector
	}

	return 0;
}

int get_all_files_recursively(string_Type_2d_Vector& my_destination_list, std::string path_from_executable)
{
	File_Class file;
	int validation;

	file.filepath = my_current_path() + constants::PATH_OF_FILE;

	system(("dir " + (string)path_from_executable + "/s /b /a > db_list.txt").c_str());

	file.number_of_elements_line = 1;
	file.delimiter = '\0';
	file.element_type = constants::ELEMENT_TYPE::STRING_TYPE;
	validation = read_file(file); //returns 1 if success else returns 0

	if (validation)
	{
		my_destination_list = file.string_parsed_rawdata;
		return 1;
	}
	else
		return 0;
}

int find_file_extension(string_Type_2d_Vector my_list_of_db_files, std::vector<File_Class>& my_dest_db_structure, constants::FILE_EXTENTION str_file_extension)
{

	constants::FILE_EXTENTION FILE_EXT;							//Used for the Switch case
	int j = 0;													//Used to index filtered file extention List


	for (std::vector<int>::size_type i = 0; i != my_list_of_db_files.size(); i++)
	{
		FILE_EXT = get_file_ext(my_list_of_db_files[i][0]);		//Get file ext

		if(FILE_EXT == str_file_extension)
		{
			my_dest_db_structure.push_back(File_Class());				//Create a new index of class File_Class
			my_dest_db_structure[j].filepath = my_list_of_db_files[i][0];
			my_dest_db_structure[j].ExpectedOutput = (int)my_dest_db_structure[j].filepath.at(my_dest_db_structure[j].filepath.size() - 6) - 48;

			if (my_dest_db_structure[j].ExpectedOutput == 63)
				my_dest_db_structure[j].ExpectedOutput = 0;

			my_dest_db_structure[j].delimiter = constants::DELIMETER;
			my_dest_db_structure[j].number_of_elements_line = constants::NUMBER_OF_DATA_LINE;
			my_dest_db_structure[j].element_type = constants::ELEMENT_TYPE::FLOAT_TYPE;
			j++;
		}
	}

	return (int)my_dest_db_structure.size();
}

constants::FILE_EXTENTION get_file_ext(std::string filename)
{

	string current_file_extention;
	current_file_extention = filename.substr(filename.find_last_of(".") + 1);


	if		(current_file_extention == constants::file_type[constants::FILE_EXTENTION::TEXT]) 
	{
		return constants::FILE_EXTENTION::TEXT;
	}
	else if (current_file_extention == constants::file_type[constants::FILE_EXTENTION::CONFIG])
	{
		return constants::FILE_EXTENTION::CONFIG;
	}
	else if (current_file_extention == constants::file_type[constants::FILE_EXTENTION::LIST])
	{
		return constants::FILE_EXTENTION::LIST;
	}
	else if (current_file_extention == constants::file_type[constants::FILE_EXTENTION::LST])
	{
		return constants::FILE_EXTENTION::LST;
	}
	else
		return constants::FILE_EXTENTION::NONE;
}

//void thread_function(std::vector<File_Class>& my_dest_db_structure, int tid, int numCPU)
//{
//	int i;
//
//	int MAX_SIZE_VECTOR = (int)my_dest_db_structure.size();	
//
//	int is_remainder = MAX_SIZE_VECTOR % numCPU;
//
//	int work_per_thread = MAX_SIZE_VECTOR / numCPU;
//
//	int begin_index = tid*work_per_thread;
//
//	if (!is_remainder)														//This condition checks if the work can be equaly devided
//	{
//		for (i = begin_index; i < (begin_index + work_per_thread); i++)
//		{
//			cout << "Thread[" << tid << "] Loading File:  " + my_dest_db_structure[i].filepath << endl;
//			read_file(my_dest_db_structure[i]);
//		}
//	}
//	else																	//This section will give the remaining work to one of the threads
//	{
//		if (tid != (numCPU - 1))
//		{
//			for (i = begin_index; i < (begin_index + work_per_thread); i++)
//			{
//				cout << "Thread[" << tid << "] Loading File:  " + my_dest_db_structure[i].filepath << endl;
//				read_file(my_dest_db_structure[i]);
//			}
//		}
//		else
//		{
//			for (i = begin_index; i < MAX_SIZE_VECTOR; i++)//This will have the last thread read until vector size
//			{
//				cout << "Thread[" << tid << "] Loading File:  " + my_dest_db_structure[i].filepath << endl;
//				read_file(my_dest_db_structure[i]);
//			}
//		}
//	}
//}

int load_neural_network_db(std::vector<File_Class>& my_dest_db_structure, bool multithread)
{

	if (!multithread)
	{
		int MAX_SIZE_VECTOR = (int)my_dest_db_structure.size();
		int percentage_done;
		for (int i = 0; i < MAX_SIZE_VECTOR; i++)
		{
			percentage_done = (i * 100 / MAX_SIZE_VECTOR);
			cout << "Thread[" << 0 << "] Loading File:  " + my_dest_db_structure[i].filepath << endl;
			cout << "(" << __func__ << ")" << "Fetching:  " << percentage_done << "% Done" << endl;
			read_file(my_dest_db_structure[i]);
		}

		return 1;
	}
	//else if (multithread)
	//{
	//	vector<std::thread> t_task;

	//	//setting up number of cores
	//	int numcpu = std::thread::hardware_concurrency();



	//	//spawning threads
	//	for (int i = 0; i < numcpu; i++)
	//		t_task.push_back(std::thread(thread_function, i, numcpu));


	//	//wait for all tasks to be done
	//	//for_each(t_task.begin(), t_task.end(), std::mem_fn(&thread::join));


	//	for (int i = 0; i < numcpu; i++)
	//		t_task[i].join();

	//	return 1;
	//}
	else
	{
		cout << __func__ << ": You have not chosen a valid multithread choice... Aborting..." << endl;
		return 0;
	}
}

int filter_neural_network(std::vector<File_Class>& my_dest_db_structure, constants::FILTERING_OPTION filter_option, constants::NUMBER_OF_SAMPLES_ACCEPTED my_samples_number)
{
	/*Shared Local Variables for all filtering options*/
	int struture_size = (int)my_dest_db_structure.size();
	int data_to_compare;
	int max_sample_number = (int)my_samples_number;

	for (int i = 0; i < struture_size; i++)
	{
		cout << "(" << __func__ << ") Filtering: " << i*100/struture_size << "% Done." << endl;					//Gives user a status of current process

		int lines_number = (int)my_dest_db_structure[i].float_parsed_rawdata.size();

		if (filter_option == constants::FILTERING_OPTION::OPTIONA || filter_option == constants::FILTERING_OPTION::OPTIONC)		//Option A and C Element Set
			data_to_compare = 25;
		else if (filter_option == constants::FILTERING_OPTION::OPTIONB)															//Option B Element Set
			data_to_compare = 12;





		if (filter_option == constants::FILTERING_OPTION::OPTIONA || filter_option == constants::FILTERING_OPTION::OPTIONB)
		{																					// Option B compares the static energy
			if (filter_option == constants::FILTERING_OPTION::OPTIONA)
			{
				cout << __func__ << ": Starting OptionA Filtering on... " << endl;
				cout << my_dest_db_structure[i].filepath << endl;
			}
			else if (filter_option == constants::FILTERING_OPTION::OPTIONB)
			{
				cout << __func__ << ": Starting OptionB Filtering on... " << endl;
				cout << my_dest_db_structure[i].filepath << endl;
			}

			if (lines_number > max_sample_number)
			{
				int data_to_erase;
				int too_much_data = lines_number - max_sample_number;
				bool still_zeros = 1;
				while (too_much_data)
				{																			//while there is more data than max_sample_number it erases the lowest energy line
					if (still_zeros)
					{																		// While there is zeros it erases them first
						for (int k = 0; k < lines_number; k++)
						{
							float probe = my_dest_db_structure[i].float_parsed_rawdata[k][data_to_compare];
							if (0 == my_dest_db_structure[i].float_parsed_rawdata[k][data_to_compare] && too_much_data != 0) {
								my_dest_db_structure[i].float_parsed_rawdata.erase(my_dest_db_structure[i].float_parsed_rawdata.begin() + k);
								too_much_data--;
								k--;														//when a line is erased all the further lines drops decrement 1 line
								lines_number--;
							}
							else if (too_much_data == 0)
							{															    // if we erased enough zeros but there are still zeros we break the loop
								break;
							}

							still_zeros = 0;
						}
					}
					else
					{																		// if all zeros are erased then we erase the lowest energy line
						for (int k = 0; k < lines_number; k++)
						{																    // this loop compares every line and keeps the lowest energy line in memory
							float lowest_energy_value = 10;

							if (lowest_energy_value >= my_dest_db_structure[i].float_parsed_rawdata[k][data_to_compare] && too_much_data != 0)
							{
								lowest_energy_value = my_dest_db_structure[i].float_parsed_rawdata[k][data_to_compare];
								data_to_erase = k;											// the data_to_erase when the loop ends is the number of the lowest energy line
							}
							else if (too_much_data == 0)
							{																// if we erased enough data, we break the loop
								break;
							}

						}																	// this line erases the line
						my_dest_db_structure[i].float_parsed_rawdata.erase(my_dest_db_structure[i].float_parsed_rawdata.begin() + data_to_erase); 
						too_much_data--;
						lines_number--;

					}
				}
			}

			if (filter_option == constants::FILTERING_OPTION::OPTIONA)
			{
				cout << __func__ << ": Finished OptionA Filtering on.. " << endl;
				cout << my_dest_db_structure[i].filepath << " containing " <<
					my_dest_db_structure[i].float_parsed_rawdata.size() << " samples" << endl;
			}
			else if (filter_option == constants::FILTERING_OPTION::OPTIONB)
			{
				cout << __func__ << ": Finished OptionB Filtering on.. " << endl;
				cout << my_dest_db_structure[i].filepath << " containing " <<
					my_dest_db_structure[i].float_parsed_rawdata.size() << " samples" << endl;
			}
		}
		else if (filter_option == constants::FILTERING_OPTION::OPTIONC)
		{

			cout << __func__ << ": Starting OptionC Filtering on... " << endl;
			cout << my_dest_db_structure[i].filepath << endl;
			
			std::vector<float> difference_list;												

																							//fetch a sorted list of delta values
			int difference_list_size = get_local_difference(difference_list, my_dest_db_structure[i].float_parsed_rawdata, data_to_compare);
			
				if ((float)max_sample_number < difference_list.size())						//Check if we actually have enough samples wanted
				{
					float lowest_threshold_difference = difference_list[(difference_list_size - max_sample_number)];


																							//Go through each difference and pull out the least significant values until we get at least max_sample_number wanted
					for (int k = 1; k <= max_sample_number; )
					{
						if (my_dest_db_structure[i].float_parsed_rawdata.size() == my_samples_number)
							break;
						else
						{
							if (lowest_threshold_difference > abs(my_dest_db_structure[i].float_parsed_rawdata[k][data_to_compare] - my_dest_db_structure[i].float_parsed_rawdata[k][data_to_compare - 1]))
								my_dest_db_structure[i].float_parsed_rawdata.erase(my_dest_db_structure[i].float_parsed_rawdata.begin() + k);
							else
								k++;
						}
					}

																							//Kill the rest since 0-39 acceptable values
					while (my_dest_db_structure[i].float_parsed_rawdata.size() > max_sample_number)
						my_dest_db_structure[i].float_parsed_rawdata.erase(my_dest_db_structure[i].float_parsed_rawdata.begin() + (my_dest_db_structure[i].float_parsed_rawdata.size() - 1));

					cout << __func__ << ": Finished OptionC Filtering on.. " << endl;
					cout << my_dest_db_structure[i].filepath << " containing " <<
						    my_dest_db_structure[i].float_parsed_rawdata.size() << " samples" << endl;
				}
				else
				{
					cout << __func__ << ": OptionC -> The quantity of samples wanted is lower than the floating size array of element [" <<
						my_dest_db_structure[i].float_parsed_rawdata.size() << "] Not modifying this index!";
				}

		}
		else																				//Default condition in case of improper use of this function
		{
			cout << __func__ << ": Invalid filtering option chosen. Aborting Filtering!";
			return 0;
		}
	}

	return 1;
}

int get_local_difference(std::vector<float>& my_difference_list, float_Type_2d_Vector my_list, int comparing_element)
{
	int lines_number = (int)my_list.size();

	for (int k = 1; k < lines_number; k++)
		my_difference_list.push_back(abs(my_list[k][comparing_element] - my_list[k][comparing_element - 1]));

	std::sort(my_difference_list.begin(), my_difference_list.end());				//Sort smallest to biggest values obtained
	std::reverse(my_difference_list.begin(), my_difference_list.end());				//Reverse so that we have Biggest to smallest values

	return (int)my_difference_list.size();
}

int write_output_db_to_txt(std::vector<File_Class>& my_source_db_structure, std::string target_file_path, std::string filter_option)
{
	ofstream target_file;
	target_file.open(target_file_path);

	for (int i = 0; i < my_source_db_structure.size(); i++)
	{
		target_file << "__________________________________________________________________" << endl;
		target_file << "FilePath: " << my_source_db_structure[i].filepath << endl;
		target_file << "Filtering Option: " << filter_option << endl;
		target_file << "Number of samples kept: " << my_source_db_structure[i].float_parsed_rawdata.size() << endl;
		target_file << "__________________________________________________________________" << endl;
		
		for (int j = 0; j < my_source_db_structure[i].float_parsed_rawdata.size(); j++)			
				target_file << my_source_db_structure[i].float_parsed_rawdata[j][25] << endl;
			

		target_file << "__________________________________________________________________" << endl;
		target_file << endl << endl;
	
	}

	cout << __func__ << ": Outputed DataFile to " << target_file_path << endl;

	target_file.close();

	return 1;
}

int get_subdirectories(std::string directory_path, std::vector<std::string>& subdirectory_list)
{
	//Referenced from http://www.cplusplus.com/forum/general/4648/
	

	string my_current_path(directory_path);
	vector<string> directory_list;
	DIR *dir = opendir(my_current_path.c_str());



	if (dir)
	{
		struct dirent *ent;
		while ((ent = readdir(dir)))
		{
			if (ent->d_type == DT_DIR)
				if ((string)ent->d_name != "." && (string)ent->d_name != "..")
					subdirectory_list.push_back(my_current_path + "\\" + ent->d_name);
		}

		return 0;
		closedir(dir);
	}
	else
	{
		cout << "Error opening directory" << endl;
		return 1;
	}
	
}

void WriteDbToBin(std::string MyPath, std::vector<File_Class>& FileData)
{
	int DbStructureSize = FileData.size();
	int SamplesPerFile  = FileData[0].float_parsed_rawdata.size();
	int ValuesPerSample = FileData[0].float_parsed_rawdata[0].size();
	int CurrentStringSize;

	fstream BinFile(MyPath, ios::out | ios::binary);

	if (BinFile.is_open())
	{	

		BinFile.write((char *)&DbStructureSize, sizeof(int));
		BinFile.write((char *)&SamplesPerFile , sizeof(int));
		BinFile.write((char *)&ValuesPerSample, sizeof(int));
		
		for (int TextFileIndex = 0; TextFileIndex < DbStructureSize; TextFileIndex++)
		{
			CurrentStringSize = FileData[TextFileIndex].filepath.size();
			BinFile.write((char *)&CurrentStringSize, sizeof(int));
			BinFile.write(FileData[TextFileIndex].filepath.c_str(), CurrentStringSize);

			BinFile.write((char *)&FileData[TextFileIndex].delimiter, sizeof(char));				
			BinFile.write((char *)&FileData[TextFileIndex].number_of_elements_line, sizeof(int));
			BinFile.write((char *)&FileData[TextFileIndex].element_type, sizeof(int));
			BinFile.write((char *)&FileData[TextFileIndex].number_of_samples, sizeof(int));
			BinFile.write((char *)&FileData[TextFileIndex].ExpectedOutput, sizeof(int));

			for (int SamplesIndex = 0; SamplesIndex < SamplesPerFile; SamplesIndex++)
				for (int ElementIndex = 0; ElementIndex < ValuesPerSample; ElementIndex++)
					BinFile.write((char *)&FileData[TextFileIndex].float_parsed_rawdata[SamplesIndex][ElementIndex], sizeof(float));

		}
	
				

		BinFile.close();
	}
	else
		cout << __func__ << ": Cannot Write to file '" << MyPath << endl;
}

void ReadDbFromBin(std::string MyPath, std::vector<File_Class>& FileData)
{
	fstream BinFile(MyPath, ios::in | ios::binary);

	int DbStructureSize;
	int SamplesPerFile;
	int ValuesPerSample;
	int CurrentStringSize;
	float TempElement;
	char *TempString;

	if (BinFile.is_open())
	{	

		BinFile.read((char *)&DbStructureSize, sizeof(int));
		BinFile.read((char *)&SamplesPerFile , sizeof(int));
		BinFile.read((char *)&ValuesPerSample, sizeof(int));
		
		FileData.resize(DbStructureSize);

		for (int TextFileIndex = 0; TextFileIndex < DbStructureSize; TextFileIndex++)
		{
			BinFile.read((char *)&CurrentStringSize, sizeof(int));
			TempString = new char[CurrentStringSize];
			BinFile.read(TempString, CurrentStringSize);
			FileData[TextFileIndex].filepath.append(TempString, CurrentStringSize);

			BinFile.read((char *)&FileData[TextFileIndex].delimiter, sizeof(char));
			BinFile.read((char *)&FileData[TextFileIndex].number_of_elements_line, sizeof(int));
			BinFile.read((char *)&FileData[TextFileIndex].element_type, sizeof(int));
			BinFile.read((char *)&FileData[TextFileIndex].number_of_samples, sizeof(int));
			BinFile.read((char *)&FileData[TextFileIndex].ExpectedOutput, sizeof(int));

			FileData[TextFileIndex].float_parsed_rawdata.resize(SamplesPerFile);


			for (int SamplesIndex = 0; SamplesIndex < SamplesPerFile; SamplesIndex++)
			{
				FileData[TextFileIndex].float_parsed_rawdata[SamplesIndex].resize(ValuesPerSample);

				for (int ElementIndex = 0; ElementIndex < ValuesPerSample; ElementIndex++)
					BinFile.read((char *)&FileData[TextFileIndex].float_parsed_rawdata[SamplesIndex][ElementIndex], sizeof(float));
				
			}
		}
	}
	else
		cout << __func__ << ": Cannot Read to file '" << MyPath << endl;
}

void GenerateTXTtoBinFromDataBase(std::string OutputBin, std::string ParentDirectory, constants::FILTERING_OPTION FilteringOption, constants::NUMBER_OF_SAMPLES_ACCEPTED SampleNumber, std::string OutPutLogPath)
{
	
	string_Type_2d_Vector db_list_to_load;
	std::vector<File_Class> db_structure;
	std::string StringOption;
	
	get_all_files_recursively(db_list_to_load, ParentDirectory);

	find_file_extension(db_list_to_load, db_structure, constants::FILE_EXTENTION::TEXT);

	load_neural_network_db(db_structure, FALSE);

	filter_neural_network(db_structure, FilteringOption, SampleNumber);

	if (FilteringOption == constants::FILTERING_OPTION::OPTIONA)
		StringOption = "a";
	else if(FilteringOption == constants::FILTERING_OPTION::OPTIONB)
		StringOption = "b";
	else if(FilteringOption == constants::FILTERING_OPTION::OPTIONC)
		StringOption = "c";

	write_output_db_to_txt(db_structure, OutPutLogPath, StringOption);

	WriteDbToBin(OutputBin, db_structure);
	
}

int DoesFileExist (const std::string& FileName) 
{
    ifstream MyFile(FileName.c_str());
    return MyFile.good();
}



