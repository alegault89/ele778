#pragma once
/*******************************************************************/
/***************Global Constants & Definitions**********************/
/*******************************************************************/

/*--------------Typedefinitions----------------*/
typedef std::vector< std::vector<float> > float_Type_2d_Vector;
typedef std::vector< std::vector<std::string> > string_Type_2d_Vector;
typedef std::string file_extension_list[];


/*---------------------------------------------*/


/*-------------Namespace Constants-------------*/

namespace constants {

	const std::string PATH_OF_FILE = "\\db_list.txt";						//Where the code stores the current database list fetched under PARENT_DIRECTORY_OF_DB recursively
	const std::string PARENT_DIRECTORY_OF_DB = "Db\\txt_dist\\train";
	const std::string PARENT_DIRECTORY_OF_DB_VC = "Db\\txt_dist\\vc\\";
	const std::string PARENT_DIRECTORY_OF_DB_TEST = "Db\\txt_dist\\test\\";
	const int NUMBER_OF_DATA_LINE = 26;										//Number of elements per line in a text file
	const float INIT_DATA_VALUES = -10;										//Please take note that values cannot be 0 since
																			//it is considered to be equivalent to no noise
	const char DELIMETER = ' ';

	enum ELEMENT_TYPE
	{
		STRING_TYPE,														//Setting File_Class.element_type with this option will tell read_file to import incoming
																			//data as string types--------
		FLOAT_TYPE															//Setting File_Class.element_type with this option will tell read_file to convert incoming
																			//Data types into float-------
	};

	enum FILTERING_OPTION
	{
		OPTIONA,															//Option A will filter by removing 0's intensity levels
		OPTIONB,															//Option B will filter by keeping highest intensity levels
		OPTIONC																//Option C will filter by using an average value threshold
	};

	enum FILE_EXTENTION
	{
		NONE,																//In case of errors
		TEXT,																//Used for file_type[] index
		CONFIG,																//Used for file_type[] index
		LST,																//Used for file_type[] index
		LIST																//Used for file_type[] index
	};
	
	
	enum NUMBER_OF_SAMPLES_ACCEPTED
	{
		NO_40_SAMPLES = 40,
		NO_50_SAMPLES = 50,
		NO_60_SAMPLES = 60
	};

	const file_extension_list file_type{ "", "txt", "config", "lst", "list" };	//example -> file_type[TEXT] == "txt"
}
/*---------------------------------------------*/





/************************************************************************/
/******************************Data Structures***************************/
/************************************************************************/
class File_Class
{
	public:
		/*The following  Paramters need to be filled in order to use it*/
		std::string filepath;												//location of the file
		char delimiter;														//character that splits each data
		int number_of_elements_line;										//number of data per line
		int element_type;													//This is the type of which you are extracting from the text file
		int number_of_samples;												//This will be used to store and save the quantity of samples taken per file
		int ExpectedOutput;
		/*-------------------------------------------------------------*/

		/*The following output vectors are determined via the element_type*/
		float_Type_2d_Vector float_parsed_rawdata;							//it's floating parsed data
		string_Type_2d_Vector string_parsed_rawdata;						//it's string parsed data
};



	
/************************************************************************/
/********************************Functions*******************************/
/************************************************************************/


/*-----------------------------------------------------------------*/
//Function Parameters	  : None	

//Function Description    : Call this function to fetch current path the exec is found in                                     
//Function Output(Success): It's current path
//Function Output(Failure): returns and empty string ""
/*-----------------------------------------------------------------*/
std::string my_current_path();

/*-----------------------------------------------------------------*/
//Function Parameters:
//							Send it a File_Class structure
//							The Paramters ".filepath - .delimeter - .number_of_elements_line" 
//							need to be initialized in order for it to work properly
//Function Description    : Call this function to fill a 2D vector by fetching your character seperated value formated document.                                           
//Function Output(Success): returns a 1 and fills up "parsed_rawdata"	
//Function Output(Failure): returns a 0 with "parsed_rawdata" empty
/*-----------------------------------------------------------------*/
int read_file(File_Class& parsing_file);

/*-----------------------------------------------------------------*/
//Function Parameters:
//							Give it a folder starting from the executable -> path_from_executable
//							upon success it will overwrite on my_destination_list
//Function Description    :	Not needed(Anthony)                                          
//Function Output(Success): returns a 1 with a filled up my_destination_list
//Function Output(Failure): returns a 0 and does not modify my_destination_list
/*-----------------------------------------------------------------*/
int get_all_files_recursively(string_Type_2d_Vector& my_destination_list, std::string path_from_executable);

/*-----------------------------------------------------------------*/
//Function Parameters:
//							Pass it your string_Type_2d_Vector list of files created from get_all_files_recursively()
//							It will write to my_dest_db_structure
//Function Description    :	                                         
//Function Output(Success): Returns the size of your my_dest_db_structure
//Function Output(Failure): Returns 0 since my_dest_db_structure was not filled, this is due to your choice of file extension. (it might not have found any)
/*-----------------------------------------------------------------*/
int find_file_extension(string_Type_2d_Vector my_list_of_db_files, std::vector<File_Class>& my_dest_db_structure, constants::FILE_EXTENTION str_file_extension);

/*-----------------------------------------------------------------*/
//Function Parameters:
//							Pass it a string file path or file name
//Function Description    :	                                         
//Function Output(Success): Returns constants::FILE_EXTENTION::*  | such that * is any of the other besides NONE
//Function Output(Failure): Returns constants::FILE_EXTENTION::NONE
/*-----------------------------------------------------------------*/
constants::FILE_EXTENTION get_file_ext(std::string filename);

/*-----------------------------------------------------------------*/
//Function Parameters:
//							Send it your FileClass vector for modifiction
//Function Description    :	                                         
//Function Output(Success): Returns 1 with a fill datastruct read for filtering
//Function Output(Failure): Returns 0 and doesn't continue filling the struct from the db. It will keep all other data
//							That was successfull
/*-----------------------------------------------------------------*/
int load_neural_network_db(std::vector<File_Class>& my_dest_db_structure, bool multithread);

/*-----------------------------------------------------------------*/
//Function Parameters:
//							my_dest_db_structure will be modified to have all txt files from database stored
//							tid will identify which thread it belongs to
//							numCPU will identify the number of CPU's your hardware has
//Function Description    : This function is to be used with function "load_neural_network_db" only, it will assign it will spawn and kill it's own threads	                                         
//Function Output(Success): None
//Function Output(Failure): None
/*-----------------------------------------------------------------*/
//void thread_function(std::vector<File_Class>& my_dest_db_structure, int tid, int numCPU);

/*-----------------------------------------------------------------*/
//Function Parameters:
//							my_dest_db_structure vector struct list that will contain filtered the output
//							filter_option set a filtered options according to constants::FILTERING_OPTION
//							my_samples_number set number of samples according to constants::NUMBER_OF_SAMPLES_ACCEPTED enum
//Function Description    :	This function will filter your "my_dest_db_structure" and shrink to it's wanted sample range with it's filtered option                             
//Function Output(Success): Returns 1 and will contain my_dest_db_structure[index].
//Function Output(Failure): 0
/*-----------------------------------------------------------------*/
int filter_neural_network(std::vector<File_Class>& my_dest_db_structure, constants::FILTERING_OPTION filter_option, constants::NUMBER_OF_SAMPLES_ACCEPTED my_samples_number);

/*-----------------------------------------------------------------*/
//Function Parameters:
//							
//Function Description    : Fetches the differences between indexes i and i-1 difference   
//							for my_list and sorts it from biggest to smallest values onto my_difference_list
//Function Output(Success): Return the size of vector my_difference_list
//Function Output(Failure): Returns 0 since the size of my_difference_list hasen't increased
/*-----------------------------------------------------------------*/
int get_local_difference(std::vector<float>& my_difference_list, float_Type_2d_Vector my_list, int comparing_element);

/*-----------------------------------------------------------------*/
//Function Parameters:
//							my_source_db_structure will be used as our database source 
//							target_file_path is used to set the full path and name of output txt file
//							filter_option us used to let the function know which filter option we have chose
//Function Description    : Function is used to grab the "my_source_db_structure" database struc and output 
//							it to "target_file_path" .txt file
//							
//Function Output(Success): returns 1and has created a text file according to "target_file_path" parameter
//Function Output(Failure): NULL
/*-----------------------------------------------------------------*/
int write_output_db_to_txt(std::vector<File_Class>& my_source_db_structure, std::string target_file_path, std::string filter_option);

/*-----------------------------------------------------------------*/
//Function Parameters:
//							**This Section uses dirent.h header**
//Function Description    :	Not needed(Anthony)                                          
//Function Output(Success): Fetches all folders in a directory(non recursive)
/*-----------------------------------------------------------------*/
int get_subdirectories(std::string directory_path, std::vector<std::string>& subdirectory_list);

/*-----------------------------------------------------------------*/
//Function Parameters:
//
//Function Description    :	                                     
//Function Output(Success): 
/*-----------------------------------------------------------------*/
void WriteDbToBin(std::string MyPath, std::vector<File_Class>& FileData);

/*-----------------------------------------------------------------*/
//Function Parameters:
//
//Function Description    :	                                     
//Function Output(Success): 
/*-----------------------------------------------------------------*/
void ReadDbFromBin(std::string MyPath, std::vector<File_Class>& FileData);

/*-----------------------------------------------------------------*/
//Function Parameters:
//
//Function Description    :	                                     
//Function Output(Success): 
/*-----------------------------------------------------------------*/
void GenerateTXTtoBinFromDataBase( std::string OutputBin, std::string ParentDirectory, 
									      constants::FILTERING_OPTION FilteringOption, 
								   constants::NUMBER_OF_SAMPLES_ACCEPTED SampleNumber, 
															std::string OutPutLogPath);

/*-----------------------------------------------------------------*/
//Function Parameters:
//
//Function Description    :	                                     
//Function Output(Success): 
/*-----------------------------------------------------------------*/
int DoesFileExist(const std::string& FileName);