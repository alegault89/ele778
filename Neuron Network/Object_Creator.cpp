/*-----------------------------Included Libraries----------------------------*/

#include <fstream>
#include <iostream>
#include <vector>
#include <Windows.h>
#include <sstream>
#include "file_access.h"
#include "NeuralNet.h"
#include <string>

using namespace std;
/*--------------------------------------------------------------------------*/




/*-----------------------------Global Variables-----------------------------*/
double DEFAULT_WEIGHT_VALUES;
double LOWER_RANDOM_BOUND;
double UPPER_RANDOM_BOUND;
int	  ULBOUNDPRECISION;
int    TRAIN_A_NETWORK;
float  LEARNING_VALUE_N;
int    NUMBEROFDATAPERSAMPLE;
int    SIZE_OF_NETWORK;
std::string CONFIG_PATH		 =   "NeuralNetwork.config";			
std::string WEIGHTS_BACKUP_PATH;
std::string LOAD_WEIGHTS_FROM_PATH;
std::string OUTPUT_LOGFILE;
INIT_WEIGHTS_FROM DEFAULT_WEIGHT_MODE;
ACTIVATION_FUNCTION ACTIVATIONFUNCTION;
int TOTALRANDOMQTY;
int ADJUSTEDLOWERBOUND;
int NUMBER_OF_ITERATIONS;


std::string TRAIN_DB_NAME;
std::string VC_DB_NAME;
std::string TEST_DB_NAME;
int FIRST_LAYER_SIZE;
constants::FILTERING_OPTION  FILTERING_OPTION_CHOSEN;
constants::NUMBER_OF_SAMPLES_ACCEPTED NUMBER_OF_SAMPLE_PER_FILE;
int SUCCESS_RATE_THRESHOLD;
int LOAD_SAVED_WEIGHTS;
/*--------------------------------------------------------------------------*/


int main(int argc, char *argv[])
{
	if (!DoesFileExist("NeuralNetwork.config"))													//Check if confi file present
	{
			cout << "The File NeuralNetwork.config cannot be found, please put the config in the same folder as this exeucatble";
			cin.ignore();
			exit(EXIT_FAILURE);
	}																							

	ShowIntroductionAndPause();																	//Give a introduction to the user



	/*--------------------------------------------------------------------------*/
	/*--------------------Local Variables used for project----------------------*/
	/*--------------------------------------------------------------------------*/
	std::vector<File_Class> TrainingDataBase;
	std::vector<File_Class> CrossValidationDataBase;
	std::vector<File_Class> TestDataBase;
	constants::FILTERING_OPTION Filtering_option_chosen;
	constants::NUMBER_OF_SAMPLES_ACCEPTED number_of_samples_chosen;
	Neuron_Network NNetwork;
	ofstream TrainingNetworkFile;
	float TrainingCurrentSuccesRate = 0, CrossValidationCurrentSuccesRate = 0, TestCurrentSuccesRate = 0, HighestSuccessRateYet = 0;
	SavedFileSettings MyFile;
	std::vector<File_Class> FileData_Test;
	int HiddenLayerSize1, HiddenLayerSize2, HiddenLayerSize3, HiddenLayerSize4;
	/*--------------------------------------------------------------------------*/


	MyFile.ReadFromConfigFile();																//Fetch Parameters from config file

	MyFile.LoadGlobalVariablesWithMyConfiguartion();											//Load parameters to global vars

	TOTALRANDOMQTY = (int)(abs(LOWER_RANDOM_BOUND*ULBOUNDPRECISION) + UPPER_RANDOM_BOUND*ULBOUNDPRECISION + 1);
	ADJUSTEDLOWERBOUND = (int)(LOWER_RANDOM_BOUND * ULBOUNDPRECISION);


	if (LOAD_SAVED_WEIGHTS)
	{
		if (!DoesFileExist(LOAD_WEIGHTS_FROM_PATH + ".bin"))
		{
			cout << "The file " << LOAD_WEIGHTS_FROM_PATH << ".bin doesn't exist, make sure you have the right file" << endl;
			cout << "And that you did not add the .bin extension to the parameter... Exiting..." << endl;
			cin.ignore();
			exit(EXIT_FAILURE);
		}

		MyFile.ReadCurrentWeightsFromBin(LOAD_WEIGHTS_FROM_PATH + ".bin", NNetwork);
		SIZE_OF_NETWORK = NNetwork.NetworkLayerNumber;
	}
	else
		NNetwork.SetNetworkLayers(SIZE_OF_NETWORK);													//Sets the size of the Neural Network


	if (!DoesFileExist(TRAIN_DB_NAME + ".bin"))													//Check if we already DataBase in bin format
	{
		GenerateTXTtoBinFromDataBase(TRAIN_DB_NAME + ".bin", "Db\\txt_dist\\train\\",
			FILTERING_OPTION_CHOSEN,
			NUMBER_OF_SAMPLE_PER_FILE,
			TRAIN_DB_NAME + ".txt");
	}

	if (!DoesFileExist(VC_DB_NAME + ".bin"))													//Check if we already DataBase in bin format
	{
		GenerateTXTtoBinFromDataBase(VC_DB_NAME + ".bin", "Db\\txt_dist\\vc\\",
			FILTERING_OPTION_CHOSEN,
			NUMBER_OF_SAMPLE_PER_FILE,
			VC_DB_NAME + ".txt");
	}

	if (!DoesFileExist(TEST_DB_NAME + ".bin"))													//Check if we already DataBase in bin format
	{
		GenerateTXTtoBinFromDataBase(TEST_DB_NAME + ".bin", "Db\\txt_dist\\test\\",
			FILTERING_OPTION_CHOSEN,
			NUMBER_OF_SAMPLE_PER_FILE,
			TEST_DB_NAME + ".txt");
	}

	cout << "Fetching Data Set" << TRAIN_DB_NAME << " From Bin" << endl;
	ReadDbFromBin(TRAIN_DB_NAME + ".bin", TrainingDataBase);									//Load the DataBase From bin file
	cout << "Fetching Data Set" << VC_DB_NAME << " From Bin" << endl;
	ReadDbFromBin(VC_DB_NAME + ".bin", CrossValidationDataBase);								//Load the DataBase From bin file
	cout << "Fetching Data Set" << TEST_DB_NAME << " From Bin" << endl;
	ReadDbFromBin(TEST_DB_NAME + ".bin", TestDataBase);											//Load the DataBase From bin file

	if (!LOAD_SAVED_WEIGHTS)
	{
		if (SIZE_OF_NETWORK == 2)
		{
			NNetwork.SetLayerSize(0, FIRST_LAYER_SIZE);													//Resize First Layer
			NNetwork.SetLayerSize(1, NUMBER_OF_OUTPUT);
		}
		else if (SIZE_OF_NETWORK == 3)
		{
			HiddenLayerSize1 = (FIRST_LAYER_SIZE + 10) / 2;

			NNetwork.SetLayerSize(0, FIRST_LAYER_SIZE);													//Resize First Layer
			NNetwork.SetLayerSize(1, HiddenLayerSize1);													//Resizes the hiddenLayer by using an average
			NNetwork.SetLayerSize(2, NUMBER_OF_OUTPUT);
		}
		else if (SIZE_OF_NETWORK == 4)
		{
			HiddenLayerSize1 = (FIRST_LAYER_SIZE + NUMBER_OF_OUTPUT) / 2;
			HiddenLayerSize2 = (FIRST_LAYER_SIZE + HiddenLayerSize1 + 10) / 4;

			NNetwork.SetLayerSize(0, FIRST_LAYER_SIZE);													//Resize First Layer
			NNetwork.SetLayerSize(1, HiddenLayerSize1);													//Resizes the hiddenLayer by using an average
			NNetwork.SetLayerSize(2, HiddenLayerSize2);													//Resizes the hiddenLayer by using an average
			NNetwork.SetLayerSize(3, NUMBER_OF_OUTPUT);
		}
		else if (SIZE_OF_NETWORK >= 5)
		{
			HiddenLayerSize1 = (FIRST_LAYER_SIZE + NUMBER_OF_OUTPUT) / 2;
			HiddenLayerSize2 = (FIRST_LAYER_SIZE + HiddenLayerSize1 + NUMBER_OF_OUTPUT) / 4;
			HiddenLayerSize3 = (FIRST_LAYER_SIZE + HiddenLayerSize1 + HiddenLayerSize2 + NUMBER_OF_OUTPUT) / 6;

			NNetwork.SetLayerSize(0, FIRST_LAYER_SIZE);													//Resize First Layer
			NNetwork.SetLayerSize(1, HiddenLayerSize1);													//Resizes the hiddenLayer by using an average
			NNetwork.SetLayerSize(2, HiddenLayerSize2);													//Resizes the hiddenLayer by using an average
			NNetwork.SetLayerSize(3, HiddenLayerSize3);													//Resizes the hiddenLayer by using an average
			NNetwork.SetLayerSize(4, NUMBER_OF_OUTPUT);
		}
	}

	NNetwork.LoadInputsFromDataBase(TrainingDataBase, 0);												//Creates a single link in the vector to be used in TrainNetwork()

	if (!LOAD_SAVED_WEIGHTS)
	{
		NNetwork.CreateNetworkWeights();																//Creates Weights according to the size network
		NNetwork.InitWeights(DEFAULT_WEIGHT_MODE);														//Fill in weights by default or by rand()
	}
		cout << "_____________________________________________________________________" << endl;
	cout << endl << endl << "Started Training Network with " << TRAIN_DB_NAME << endl;
	cout << "Configuration: " << endl <<
		"                     Activation Function : " << ACTIVATIONFUNCTION << endl
		<< "                     Weight Values Mode  : " << DEFAULT_WEIGHT_MODE << endl
		<< "                     Default Values		 : " << DEFAULT_WEIGHT_VALUES << endl
		<< "                     Network Size        : " << SIZE_OF_NETWORK << endl
		<< "                     Number of elements/s: " << NUMBEROFDATAPERSAMPLE << endl
		<< "                     Learning Value (N)  : " << LEARNING_VALUE_N << endl
		<< "                     Rand LWR Value      : " << LOWER_RANDOM_BOUND << endl
		<< "                     Rand UPPER Value    : " << UPPER_RANDOM_BOUND << endl
		<< "                     Output Layer Size   : " << NUMBER_OF_OUTPUT << endl
		<< " (Check config file for details on params)" << endl << endl;
	cout << "_____________________________________________________________________" << endl;


	TrainingNetworkFile.open(OUTPUT_LOGFILE +".txt", std::ios::app);
	TrainingNetworkFile << "_____________________________________________________________________" << endl;
	TrainingNetworkFile << endl << endl << "Started Training Network with " << TRAIN_DB_NAME << endl;
	TrainingNetworkFile << "Configuration: " << endl <<
		"                     Activation Function : " << ACTIVATIONFUNCTION << endl
		<< "                     Weight Values Mode  : " << DEFAULT_WEIGHT_MODE << endl
		<< "                     Default Values		 : " << DEFAULT_WEIGHT_VALUES << endl
		<< "                     Network Size        : " << SIZE_OF_NETWORK << endl
		<< "                     Number of elements/s: " << NUMBEROFDATAPERSAMPLE << endl
		<< "                     Learning Value (N)  : " << LEARNING_VALUE_N << endl
		<< "                     Rand LWR Value      : " << LOWER_RANDOM_BOUND << endl
		<< "                     Rand UPPER Value    : " << UPPER_RANDOM_BOUND << endl
		<< "                     Output Layer Size   : " << NUMBER_OF_OUTPUT << endl
		<< " (Check config file for details on params)" << endl << endl;

	TrainingNetworkFile.close();

	for (int i = 0; i < NUMBER_OF_ITERATIONS; i++)																	//Main training loop
	{
		
		cout << "Started Training..." << endl;
		NNetwork.NumberOfTrainingSucceededOutput = 0;												//Trains Network
		NNetwork.TrainNetwork(TrainingDataBase, MyFile);																	
		TrainingCurrentSuccesRate = (float)(100 * (float)NNetwork.NumberOfTrainingSucceededOutput / (float)TrainingDataBase.size());
		cout << "Done Training..." << endl;

		cout << "Started Cross Validation..." << endl;
		NNetwork.NumberOfCrossValidationSucceededOutput = 0;										//Uses Cross-Validation at this stage
		NNetwork.TestNetwork(CrossValidationDataBase, MyFile);
		CrossValidationCurrentSuccesRate = (float)(100 * (float)NNetwork.NumberOfCrossValidationSucceededOutput / (float)CrossValidationDataBase.size());
		cout << "Finished Cross Validation..." << endl;

		cout << "Started Test..." << endl;
		NNetwork.NumberOfTestSucceededOutput = 0;													//Testing the Network at this stage
		NNetwork.TestNetwork(TestDataBase, MyFile);
		TestCurrentSuccesRate = (float)(100 * (float)NNetwork.NumberOfTestSucceededOutput / (float)TestDataBase.size());
		cout << "Finished Test..." << endl;
		


		if (SIZE_OF_NETWORK == 2)
		{
			cout << "L1(" << std::to_string(FIRST_LAYER_SIZE) << ") OUT(" << std::to_string(NUMBER_OF_OUTPUT) << ") Training Iteration: " << i
			<< " Learning Success Rate= " << TrainingCurrentSuccesRate << " %"
			<< " Cross Validation Success Rate= " << CrossValidationCurrentSuccesRate << " %"
			<< " Test Success Rate= " << TestCurrentSuccesRate << " %" << endl;

			TrainingNetworkFile.open(OUTPUT_LOGFILE + ".txt" , std::ios::app);
				TrainingNetworkFile << "L1(" << std::to_string(FIRST_LAYER_SIZE) << ") OUT(" << std::to_string(NUMBER_OF_OUTPUT) << ") Training Iteration: " << i
				<< " Learning Success Rate= " << TrainingCurrentSuccesRate << " %"
				<< " Cross Validation Success Rate= " << CrossValidationCurrentSuccesRate << " %"
				<< " Test Success Rate= " << TestCurrentSuccesRate << " %" << endl;
			TrainingNetworkFile.close();

		}
		else if (SIZE_OF_NETWORK == 3)
		{
			if (LOAD_SAVED_WEIGHTS)
				HiddenLayerSize1 = NNetwork.hiddenLayers[0].size();

			cout << "L1(" << FIRST_LAYER_SIZE << ") L2(" << HiddenLayerSize1 << ") OUT(" << NUMBER_OF_OUTPUT  << ") Training Iteration: " << i
			<< " Learning Success Rate= " << TrainingCurrentSuccesRate << " %"
			<< " Cross Validation Success Rate= " << CrossValidationCurrentSuccesRate << " %"
			<< " Test Success Rate= " << TestCurrentSuccesRate << " %" << endl;

			TrainingNetworkFile.open(OUTPUT_LOGFILE + ".txt" , std::ios::app);
				TrainingNetworkFile << "L1(" << FIRST_LAYER_SIZE << ") L2(" << HiddenLayerSize1 << ") OUT(" << NUMBER_OF_OUTPUT  << ") Training Iteration: " << i
				<< " Learning Success Rate= " << TrainingCurrentSuccesRate << " %"
				<< " Cross Validation Success Rate= " << CrossValidationCurrentSuccesRate << " %"
				<< " Test Success Rate= " << TestCurrentSuccesRate << " %" << endl;
			TrainingNetworkFile.close();
		}
		else if (SIZE_OF_NETWORK == 4)
		{
			if (LOAD_SAVED_WEIGHTS)
			{
				HiddenLayerSize1 = NNetwork.hiddenLayers[0].size();
				HiddenLayerSize2 = NNetwork.hiddenLayers[1].size();
			}

			cout << "L1(" << FIRST_LAYER_SIZE << ") L2(" << HiddenLayerSize1 << ") L3(" << HiddenLayerSize2 << ") OUT(" << NUMBER_OF_OUTPUT  << ") Training Iteration: " << i
			<< " Learning Success Rate= " << TrainingCurrentSuccesRate << " %"
			<< " Cross Validation Success Rate= " << CrossValidationCurrentSuccesRate << " %"
			<< " Test Success Rate= " << TestCurrentSuccesRate << " %" << endl;

			TrainingNetworkFile.open(OUTPUT_LOGFILE + ".txt" , std::ios::app);
				TrainingNetworkFile << "L1(" << FIRST_LAYER_SIZE << ") L2(" << HiddenLayerSize1 << ") L3(" << HiddenLayerSize2 << ") OUT(" << NUMBER_OF_OUTPUT  << ") Training Iteration: " << i
				<< " Learning Success Rate= " << TrainingCurrentSuccesRate << " %"
				<< " Cross Validation Success Rate= " << CrossValidationCurrentSuccesRate << " %"
				<< " Test Success Rate= " << TestCurrentSuccesRate << " %" << endl;
			TrainingNetworkFile.close();
		}
		else if (SIZE_OF_NETWORK >= 5)
		{
			if (LOAD_SAVED_WEIGHTS)
			{
				HiddenLayerSize1 = NNetwork.hiddenLayers[0].size();
				HiddenLayerSize2 = NNetwork.hiddenLayers[1].size();
				HiddenLayerSize3 = NNetwork.hiddenLayers[2].size();
			}

			cout << "L1(" << FIRST_LAYER_SIZE << ") L2(" << HiddenLayerSize1 << ") L3(" << HiddenLayerSize2 << ") L4(" << HiddenLayerSize3 << ") OUT(" << NUMBER_OF_OUTPUT  << ") Training Iteration: " << i
			<< " Learning Success Rate= " << TrainingCurrentSuccesRate << " %"
			<< " Cross Validation Success Rate= " << CrossValidationCurrentSuccesRate << " %"
			<< " Test Success Rate= " << TestCurrentSuccesRate << " %" << endl;

			TrainingNetworkFile.open(OUTPUT_LOGFILE + ".txt" , std::ios::app);
				TrainingNetworkFile << "L1(" << FIRST_LAYER_SIZE << ") L2(" << HiddenLayerSize1 << ") L3(" << HiddenLayerSize2 << ") L4(" << HiddenLayerSize3 << ") OUT(" << NUMBER_OF_OUTPUT  << ") Training Iteration: " << i
				<< " Learning Success Rate= " << TrainingCurrentSuccesRate << " %"
				<< " Cross Validation Success Rate= " << CrossValidationCurrentSuccesRate << " %"
				<< " Test Success Rate= " << TestCurrentSuccesRate << " %" << endl;
			TrainingNetworkFile.close();
		}

		if (CrossValidationCurrentSuccesRate > SUCCESS_RATE_THRESHOLD)									//Check if we've over learned and bails out, saving the weights
		{
			MyFile.WriteCurrentWeightsToBin(TRAIN_DB_NAME + "SavedWeightsFor(" + OUTPUT_LOGFILE + ").bin"	, NNetwork);
			exit(EXIT_SUCCESS);
		}
	}

	return 0;

}



