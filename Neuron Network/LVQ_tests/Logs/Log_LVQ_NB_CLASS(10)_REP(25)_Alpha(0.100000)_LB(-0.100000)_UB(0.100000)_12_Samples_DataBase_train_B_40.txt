_____________________________________________________________________


Started Training Network with DataBase_train_B_40
Configuration: 
                     Number of elements/s: 12
                     Alpha				 : 0.1
                     Rand LWR Value      : -0.1
                     Rand UPPER Value    : 0.1
                     Output Layer Size   : 10
 (Check config file for details on params)

CLASS(10) REP(25) Training Iteration: 0 Learning Success Rate= 0.794776 % Cross Validation Success Rate= 0.675 % Test Success Rate= 0.774359 %
CLASS(10) REP(25) Training Iteration: 1 Learning Success Rate= 0.847015 % Cross Validation Success Rate= 0.783333 % Test Success Rate= 0.769231 %
CLASS(10) REP(25) Training Iteration: 2 Learning Success Rate= 0.876866 % Cross Validation Success Rate= 0.75 % Test Success Rate= 0.771795 %
CLASS(10) REP(25) Training Iteration: 3 Learning Success Rate= 0.877612 % Cross Validation Success Rate= 0.708333 % Test Success Rate= 0.79359 %
CLASS(10) REP(25) Training Iteration: 4 Learning Success Rate= 0.862687 % Cross Validation Success Rate= 0.7 % Test Success Rate= 0.8 %
CLASS(10) REP(25) Training Iteration: 5 Learning Success Rate= 0.857463 % Cross Validation Success Rate= 0.741667 % Test Success Rate= 0.80641 %
CLASS(10) REP(25) Training Iteration: 6 Learning Success Rate= 0.876119 % Cross Validation Success Rate= 0.791667 % Test Success Rate= 0.821795 %
CLASS(10) REP(25) Training Iteration: 7 Learning Success Rate= 0.872388 % Cross Validation Success Rate= 0.8 % Test Success Rate= 0.788462 %
CLASS(10) REP(25) Training Iteration: 8 Learning Success Rate= 0.895522 % Cross Validation Success Rate= 0.783333 % Test Success Rate= 0.832051 %
CLASS(10) REP(25) Training Iteration: 9 Learning Success Rate= 0.879104 % Cross Validation Success Rate= 0.833333 % Test Success Rate= 0.829487 %
CLASS(10) REP(25) Training Iteration: 10 Learning Success Rate= 0.891045 % Cross Validation Success Rate= 0.775 % Test Success Rate= 0.817949 %
CLASS(10) REP(25) Training Iteration: 11 Learning Success Rate= 0.884328 % Cross Validation Success Rate= 0.825 % Test Success Rate= 0.791026 %
CLASS(10) REP(25) Training Iteration: 12 Learning Success Rate= 0.877612 % Cross Validation Success Rate= 0.858333 % Test Success Rate= 0.791026 %
CLASS(10) REP(25) Training Iteration: 13 Learning Success Rate= 0.884328 % Cross Validation Success Rate= 0.791667 % Test Success Rate= 0.794872 %
CLASS(10) REP(25) Training Iteration: 14 Learning Success Rate= 0.902239 % Cross Validation Success Rate= 0.816667 % Test Success Rate= 0.808974 %
CLASS(10) REP(25) Training Iteration: 15 Learning Success Rate= 0.884328 % Cross Validation Success Rate= 0.841667 % Test Success Rate= 0.833333 %
CLASS(10) REP(25) Training Iteration: 16 Learning Success Rate= 0.891791 % Cross Validation Success Rate= 0.891667 % Test Success Rate= 0.864103 %
CLASS(10) REP(25) Training Iteration: 17 Learning Success Rate= 0.891791 % Cross Validation Success Rate= 0.866667 % Test Success Rate= 0.829487 %
CLASS(10) REP(25) Training Iteration: 18 Learning Success Rate= 0.906716 % Cross Validation Success Rate= 0.833333 % Test Success Rate= 0.821795 %
CLASS(10) REP(25) Training Iteration: 19 Learning Success Rate= 0.890298 % Cross Validation Success Rate= 0.875 % Test Success Rate= 0.835897 %
_____________________________________________________________________


Started Training Network with DataBase_train_B_40
Configuration: 
                     Number of elements/s: 12
                     Alpha				 : 0.1
                     Rand LWR Value      : -0.1
                     Rand UPPER Value    : 0.1
                     Output Layer Size   : 10
 (Check config file for details on params)

