_____________________________________________________________________


Started Training Network with DataBase_train_C_50
Configuration: 
                     Number of elements/s: 12
                     Alpha				 : 0.1
                     Rand LWR Value      : -0.1
                     Rand UPPER Value    : 0.1
                     Output Layer Size   : 10
 (Check config file for details on params)

CLASS(10) REP(15) Training Iteration: 0 Learning Success Rate= 0.78209 % Cross Validation Success Rate= 0.733333 % Test Success Rate= 0.75 %
CLASS(10) REP(15) Training Iteration: 1 Learning Success Rate= 0.841791 % Cross Validation Success Rate= 0.825 % Test Success Rate= 0.8 %
CLASS(10) REP(15) Training Iteration: 2 Learning Success Rate= 0.84403 % Cross Validation Success Rate= 0.75 % Test Success Rate= 0.760256 %
CLASS(10) REP(15) Training Iteration: 3 Learning Success Rate= 0.876119 % Cross Validation Success Rate= 0.85 % Test Success Rate= 0.825641 %
CLASS(10) REP(15) Training Iteration: 4 Learning Success Rate= 0.878358 % Cross Validation Success Rate= 0.741667 % Test Success Rate= 0.814103 %
CLASS(10) REP(15) Training Iteration: 5 Learning Success Rate= 0.876119 % Cross Validation Success Rate= 0.816667 % Test Success Rate= 0.778205 %
CLASS(10) REP(15) Training Iteration: 6 Learning Success Rate= 0.89403 % Cross Validation Success Rate= 0.858333 % Test Success Rate= 0.865385 %
CLASS(10) REP(15) Training Iteration: 7 Learning Success Rate= 0.897015 % Cross Validation Success Rate= 0.758333 % Test Success Rate= 0.802564 %
CLASS(10) REP(15) Training Iteration: 8 Learning Success Rate= 0.907463 % Cross Validation Success Rate= 0.733333 % Test Success Rate= 0.811538 %
CLASS(10) REP(15) Training Iteration: 9 Learning Success Rate= 0.885075 % Cross Validation Success Rate= 0.833333 % Test Success Rate= 0.807692 %
CLASS(10) REP(15) Training Iteration: 10 Learning Success Rate= 0.875373 % Cross Validation Success Rate= 0.808333 % Test Success Rate= 0.783333 %
CLASS(10) REP(15) Training Iteration: 11 Learning Success Rate= 0.893284 % Cross Validation Success Rate= 0.741667 % Test Success Rate= 0.798718 %
CLASS(10) REP(15) Training Iteration: 12 Learning Success Rate= 0.886567 % Cross Validation Success Rate= 0.825 % Test Success Rate= 0.794872 %
CLASS(10) REP(15) Training Iteration: 13 Learning Success Rate= 0.897015 % Cross Validation Success Rate= 0.791667 % Test Success Rate= 0.816667 %
CLASS(10) REP(15) Training Iteration: 14 Learning Success Rate= 0.909701 % Cross Validation Success Rate= 0.875 % Test Success Rate= 0.80641 %
CLASS(10) REP(15) Training Iteration: 15 Learning Success Rate= 0.898507 % Cross Validation Success Rate= 0.841667 % Test Success Rate= 0.808974 %
CLASS(10) REP(15) Training Iteration: 16 Learning Success Rate= 0.913433 % Cross Validation Success Rate= 0.841667 % Test Success Rate= 0.791026 %
CLASS(10) REP(15) Training Iteration: 17 Learning Success Rate= 0.9 % Cross Validation Success Rate= 0.858333 % Test Success Rate= 0.808974 %
CLASS(10) REP(15) Training Iteration: 18 Learning Success Rate= 0.905224 % Cross Validation Success Rate= 0.841667 % Test Success Rate= 0.80641 %
CLASS(10) REP(15) Training Iteration: 19 Learning Success Rate= 0.914179 % Cross Validation Success Rate= 0.8 % Test Success Rate= 0.8 %
