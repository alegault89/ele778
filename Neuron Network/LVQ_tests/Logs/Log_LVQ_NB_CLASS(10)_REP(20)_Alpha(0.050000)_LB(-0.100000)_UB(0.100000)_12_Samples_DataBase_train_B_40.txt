_____________________________________________________________________


Started Training Network with DataBase_train_B_40
Configuration: 
                     Number of elements/s: 12
                     Alpha				 : 0.05
                     Rand LWR Value      : -0.1
                     Rand UPPER Value    : 0.1
                     Output Layer Size   : 10
 (Check config file for details on params)

CLASS(10) REP(20) Training Iteration: 0 Learning Success Rate= 0.756716 % Cross Validation Success Rate= 0.691667 % Test Success Rate= 0.752564 %
CLASS(10) REP(20) Training Iteration: 1 Learning Success Rate= 0.81194 % Cross Validation Success Rate= 0.808333 % Test Success Rate= 0.819231 %
CLASS(10) REP(20) Training Iteration: 2 Learning Success Rate= 0.856716 % Cross Validation Success Rate= 0.758333 % Test Success Rate= 0.79359 %
CLASS(10) REP(20) Training Iteration: 3 Learning Success Rate= 0.864925 % Cross Validation Success Rate= 0.7 % Test Success Rate= 0.811538 %
CLASS(10) REP(20) Training Iteration: 4 Learning Success Rate= 0.858955 % Cross Validation Success Rate= 0.75 % Test Success Rate= 0.828205 %
CLASS(10) REP(20) Training Iteration: 5 Learning Success Rate= 0.874627 % Cross Validation Success Rate= 0.791667 % Test Success Rate= 0.803846 %
CLASS(10) REP(20) Training Iteration: 6 Learning Success Rate= 0.88209 % Cross Validation Success Rate= 0.75 % Test Success Rate= 0.834615 %
CLASS(10) REP(20) Training Iteration: 7 Learning Success Rate= 0.88209 % Cross Validation Success Rate= 0.783333 % Test Success Rate= 0.817949 %
CLASS(10) REP(20) Training Iteration: 8 Learning Success Rate= 0.908955 % Cross Validation Success Rate= 0.783333 % Test Success Rate= 0.847436 %
CLASS(10) REP(20) Training Iteration: 9 Learning Success Rate= 0.89403 % Cross Validation Success Rate= 0.775 % Test Success Rate= 0.808974 %
CLASS(10) REP(20) Training Iteration: 10 Learning Success Rate= 0.884328 % Cross Validation Success Rate= 0.725 % Test Success Rate= 0.820513 %
CLASS(10) REP(20) Training Iteration: 11 Learning Success Rate= 0.881343 % Cross Validation Success Rate= 0.783333 % Test Success Rate= 0.817949 %
CLASS(10) REP(20) Training Iteration: 12 Learning Success Rate= 0.883582 % Cross Validation Success Rate= 0.825 % Test Success Rate= 0.785897 %
CLASS(10) REP(20) Training Iteration: 13 Learning Success Rate= 0.885075 % Cross Validation Success Rate= 0.775 % Test Success Rate= 0.820513 %
CLASS(10) REP(20) Training Iteration: 14 Learning Success Rate= 0.897015 % Cross Validation Success Rate= 0.783333 % Test Success Rate= 0.823077 %
CLASS(10) REP(20) Training Iteration: 15 Learning Success Rate= 0.893284 % Cross Validation Success Rate= 0.825 % Test Success Rate= 0.81282 %
CLASS(10) REP(20) Training Iteration: 16 Learning Success Rate= 0.895522 % Cross Validation Success Rate= 0.833333 % Test Success Rate= 0.833333 %
CLASS(10) REP(20) Training Iteration: 17 Learning Success Rate= 0.898507 % Cross Validation Success Rate= 0.833333 % Test Success Rate= 0.814103 %
CLASS(10) REP(20) Training Iteration: 18 Learning Success Rate= 0.899254 % Cross Validation Success Rate= 0.8 % Test Success Rate= 0.816667 %
CLASS(10) REP(20) Training Iteration: 19 Learning Success Rate= 0.891791 % Cross Validation Success Rate= 0.883333 % Test Success Rate= 0.824359 %
