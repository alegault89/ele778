_____________________________________________________________________


Started Training Network with DataBase_train_B_40
Configuration: 
                     Number of elements/s: 12
                     Alpha				 : 0.5
                     Rand LWR Value      : -0.1
                     Rand UPPER Value    : 0.1
                     Output Layer Size   : 10
 (Check config file for details on params)

CLASS(10) REP(25) Training Iteration: 0 Learning Success Rate= 0.704478 % Cross Validation Success Rate= 0.575 % Test Success Rate= 0.526923 %
CLASS(10) REP(25) Training Iteration: 1 Learning Success Rate= 0.666418 % Cross Validation Success Rate= 0.591667 % Test Success Rate= 0.588462 %
CLASS(10) REP(25) Training Iteration: 2 Learning Success Rate= 0.641045 % Cross Validation Success Rate= 0.533333 % Test Success Rate= 0.487179 %
CLASS(10) REP(25) Training Iteration: 3 Learning Success Rate= 0.587313 % Cross Validation Success Rate= 0.466667 % Test Success Rate= 0.448718 %
CLASS(10) REP(25) Training Iteration: 4 Learning Success Rate= 0.553731 % Cross Validation Success Rate= 0.241667 % Test Success Rate= 0.269231 %
CLASS(10) REP(25) Training Iteration: 5 Learning Success Rate= 0.481343 % Cross Validation Success Rate= 0.691667 % Test Success Rate= 0.596154 %
CLASS(10) REP(25) Training Iteration: 6 Learning Success Rate= 0.402239 % Cross Validation Success Rate= 0.566667 % Test Success Rate= 0.551282 %
CLASS(10) REP(25) Training Iteration: 7 Learning Success Rate= 0.307463 % Cross Validation Success Rate= 0.625 % Test Success Rate= 0.462821 %
CLASS(10) REP(25) Training Iteration: 8 Learning Success Rate= 0.267164 % Cross Validation Success Rate= 0.125 % Test Success Rate= 0.105128 %
CLASS(10) REP(25) Training Iteration: 9 Learning Success Rate= 0.223134 % Cross Validation Success Rate= 0.225 % Test Success Rate= 0.262821 %
CLASS(10) REP(25) Training Iteration: 10 Learning Success Rate= 0.158955 % Cross Validation Success Rate= 0.075 % Test Success Rate= 0.0858974 %
CLASS(10) REP(25) Training Iteration: 11 Learning Success Rate= 0.135821 % Cross Validation Success Rate= 0.0833333 % Test Success Rate= 0.141026 %
CLASS(10) REP(25) Training Iteration: 12 Learning Success Rate= 0.104478 % Cross Validation Success Rate= 0.05 % Test Success Rate= 0.115385 %
CLASS(10) REP(25) Training Iteration: 13 Learning Success Rate= 0.102239 % Cross Validation Success Rate= 0.158333 % Test Success Rate= 0.0974359 %
CLASS(10) REP(25) Training Iteration: 14 Learning Success Rate= 0.0977612 % Cross Validation Success Rate= 0.116667 % Test Success Rate= 0.0923077 %
CLASS(10) REP(25) Training Iteration: 15 Learning Success Rate= 0.108955 % Cross Validation Success Rate= 0.0916667 % Test Success Rate= 0.0923077 %
CLASS(10) REP(25) Training Iteration: 16 Learning Success Rate= 0.0977612 % Cross Validation Success Rate= 0.125 % Test Success Rate= 0.0923077 %
CLASS(10) REP(25) Training Iteration: 17 Learning Success Rate= 0.0932836 % Cross Validation Success Rate= 0.075 % Test Success Rate= 0.0948718 %
CLASS(10) REP(25) Training Iteration: 18 Learning Success Rate= 0.0895522 % Cross Validation Success Rate= 0.075 % Test Success Rate= 0.117949 %
CLASS(10) REP(25) Training Iteration: 19 Learning Success Rate= 0.112687 % Cross Validation Success Rate= 0.0916667 % Test Success Rate= 0.0897436 %
