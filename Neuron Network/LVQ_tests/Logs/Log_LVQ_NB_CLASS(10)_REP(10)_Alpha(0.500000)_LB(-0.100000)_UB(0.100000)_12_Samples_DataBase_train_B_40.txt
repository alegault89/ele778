_____________________________________________________________________


Started Training Network with DataBase_train_B_40
Configuration: 
                     Number of elements/s: 12
                     Alpha				 : 0.5
                     Rand LWR Value      : -0.1
                     Rand UPPER Value    : 0.1
                     Output Layer Size   : 10
 (Check config file for details on params)

CLASS(10) REP(10) Training Iteration: 0 Learning Success Rate= 0.445522 % Cross Validation Success Rate= 0.1 % Test Success Rate= 0.0846154 %
CLASS(10) REP(10) Training Iteration: 1 Learning Success Rate= 0.185075 % Cross Validation Success Rate= 0.0916667 % Test Success Rate= 0.108974 %
CLASS(10) REP(10) Training Iteration: 2 Learning Success Rate= 0.103731 % Cross Validation Success Rate= 0.0916667 % Test Success Rate= 0.0846154 %
CLASS(10) REP(10) Training Iteration: 3 Learning Success Rate= 0.122388 % Cross Validation Success Rate= 0.108333 % Test Success Rate= 0.0948718 %
CLASS(10) REP(10) Training Iteration: 4 Learning Success Rate= 0.105224 % Cross Validation Success Rate= 0.0833333 % Test Success Rate= 0.1 %
CLASS(10) REP(10) Training Iteration: 5 Learning Success Rate= 0.120149 % Cross Validation Success Rate= 0.1 % Test Success Rate= 0.0769231 %
CLASS(10) REP(10) Training Iteration: 6 Learning Success Rate= 0.0910448 % Cross Validation Success Rate= 0.116667 % Test Success Rate= 0.107692 %
CLASS(10) REP(10) Training Iteration: 7 Learning Success Rate= 0.0977612 % Cross Validation Success Rate= 0.075 % Test Success Rate= 0.102564 %
CLASS(10) REP(10) Training Iteration: 8 Learning Success Rate= 0.106716 % Cross Validation Success Rate= 0.1 % Test Success Rate= 0.0923077 %
CLASS(10) REP(10) Training Iteration: 9 Learning Success Rate= 0.100746 % Cross Validation Success Rate= 0.0666667 % Test Success Rate= 0.108974 %
CLASS(10) REP(10) Training Iteration: 10 Learning Success Rate= 0.0820896 % Cross Validation Success Rate= 0.133333 % Test Success Rate= 0.108974 %
CLASS(10) REP(10) Training Iteration: 11 Learning Success Rate= 0.104478 % Cross Validation Success Rate= 0.108333 % Test Success Rate= 0.10641 %
CLASS(10) REP(10) Training Iteration: 12 Learning Success Rate= 0.0977612 % Cross Validation Success Rate= 0.075 % Test Success Rate= 0.0897436 %
CLASS(10) REP(10) Training Iteration: 13 Learning Success Rate= 0.10597 % Cross Validation Success Rate= 0.116667 % Test Success Rate= 0.108974 %
CLASS(10) REP(10) Training Iteration: 14 Learning Success Rate= 0.0955224 % Cross Validation Success Rate= 0.1 % Test Success Rate= 0.0923077 %
CLASS(10) REP(10) Training Iteration: 15 Learning Success Rate= 0.102985 % Cross Validation Success Rate= 0.1 % Test Success Rate= 0.0820513 %
CLASS(10) REP(10) Training Iteration: 16 Learning Success Rate= 0.106716 % Cross Validation Success Rate= 0.0916667 % Test Success Rate= 0.101282 %
CLASS(10) REP(10) Training Iteration: 17 Learning Success Rate= 0.0992537 % Cross Validation Success Rate= 0.0916667 % Test Success Rate= 0.105128 %
CLASS(10) REP(10) Training Iteration: 18 Learning Success Rate= 0.091791 % Cross Validation Success Rate= 0.0916667 % Test Success Rate= 0.0948718 %
CLASS(10) REP(10) Training Iteration: 19 Learning Success Rate= 0.0858209 % Cross Validation Success Rate= 0.075 % Test Success Rate= 0.0923077 %
