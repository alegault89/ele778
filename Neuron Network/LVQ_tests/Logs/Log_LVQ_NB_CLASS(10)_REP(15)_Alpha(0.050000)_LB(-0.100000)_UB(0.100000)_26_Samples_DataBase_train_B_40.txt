_____________________________________________________________________


Started Training Network with DataBase_train_B_40
Configuration: 
                     Number of elements/s: 26
                     Alpha				 : 0.05
                     Rand LWR Value      : -0.1
                     Rand UPPER Value    : 0.1
                     Output Layer Size   : 10
 (Check config file for details on params)

CLASS(10) REP(15) Training Iteration: 0 Learning Success Rate= 0.741045 % Cross Validation Success Rate= 0.633333 % Test Success Rate= 0.728205 %
CLASS(10) REP(15) Training Iteration: 1 Learning Success Rate= 0.797015 % Cross Validation Success Rate= 0.833333 % Test Success Rate= 0.788462 %
CLASS(10) REP(15) Training Iteration: 2 Learning Success Rate= 0.824627 % Cross Validation Success Rate= 0.758333 % Test Success Rate= 0.779487 %
CLASS(10) REP(15) Training Iteration: 3 Learning Success Rate= 0.842537 % Cross Validation Success Rate= 0.725 % Test Success Rate= 0.782051 %
CLASS(10) REP(15) Training Iteration: 4 Learning Success Rate= 0.847761 % Cross Validation Success Rate= 0.758333 % Test Success Rate= 0.814103 %
CLASS(10) REP(15) Training Iteration: 5 Learning Success Rate= 0.85597 % Cross Validation Success Rate= 0.816667 % Test Success Rate= 0.803846 %
CLASS(10) REP(15) Training Iteration: 6 Learning Success Rate= 0.864179 % Cross Validation Success Rate= 0.833333 % Test Success Rate= 0.825641 %
CLASS(10) REP(15) Training Iteration: 7 Learning Success Rate= 0.858209 % Cross Validation Success Rate= 0.783333 % Test Success Rate= 0.784615 %
CLASS(10) REP(15) Training Iteration: 8 Learning Success Rate= 0.872388 % Cross Validation Success Rate= 0.741667 % Test Success Rate= 0.776923 %
CLASS(10) REP(15) Training Iteration: 9 Learning Success Rate= 0.851493 % Cross Validation Success Rate= 0.783333 % Test Success Rate= 0.771795 %
CLASS(10) REP(15) Training Iteration: 10 Learning Success Rate= 0.861194 % Cross Validation Success Rate= 0.8 % Test Success Rate= 0.778205 %
CLASS(10) REP(15) Training Iteration: 11 Learning Success Rate= 0.839552 % Cross Validation Success Rate= 0.8 % Test Success Rate= 0.761538 %
CLASS(10) REP(15) Training Iteration: 12 Learning Success Rate= 0.847761 % Cross Validation Success Rate= 0.833333 % Test Success Rate= 0.755128 %
CLASS(10) REP(15) Training Iteration: 13 Learning Success Rate= 0.846269 % Cross Validation Success Rate= 0.816667 % Test Success Rate= 0.789744 %
CLASS(10) REP(15) Training Iteration: 14 Learning Success Rate= 0.869403 % Cross Validation Success Rate= 0.766667 % Test Success Rate= 0.814103 %
CLASS(10) REP(15) Training Iteration: 15 Learning Success Rate= 0.870149 % Cross Validation Success Rate= 0.816667 % Test Success Rate= 0.784615 %
CLASS(10) REP(15) Training Iteration: 16 Learning Success Rate= 0.882836 % Cross Validation Success Rate= 0.85 % Test Success Rate= 0.816667 %
CLASS(10) REP(15) Training Iteration: 17 Learning Success Rate= 0.876866 % Cross Validation Success Rate= 0.9 % Test Success Rate= 0.826923 %
CLASS(10) REP(15) Training Iteration: 18 Learning Success Rate= 0.877612 % Cross Validation Success Rate= 0.808333 % Test Success Rate= 0.81282 %
CLASS(10) REP(15) Training Iteration: 19 Learning Success Rate= 0.861194 % Cross Validation Success Rate= 0.883333 % Test Success Rate= 0.807692 %
