_____________________________________________________________________


Started Training Network with DataBase_train_B_40
Configuration: 
                     Number of elements/s: 12
                     Alpha				 : 0.01
                     Rand LWR Value      : -0.1
                     Rand UPPER Value    : 0.1
                     Output Layer Size   : 10
 (Check config file for details on params)

CLASS(10) REP(10) Training Iteration: 0 Learning Success Rate= 0.667164 % Cross Validation Success Rate= 0.575 % Test Success Rate= 0.664103 %
CLASS(10) REP(10) Training Iteration: 1 Learning Success Rate= 0.692537 % Cross Validation Success Rate= 0.708333 % Test Success Rate= 0.741026 %
CLASS(10) REP(10) Training Iteration: 2 Learning Success Rate= 0.75 % Cross Validation Success Rate= 0.733333 % Test Success Rate= 0.703846 %
CLASS(10) REP(10) Training Iteration: 3 Learning Success Rate= 0.764179 % Cross Validation Success Rate= 0.683333 % Test Success Rate= 0.751282 %
CLASS(10) REP(10) Training Iteration: 4 Learning Success Rate= 0.769403 % Cross Validation Success Rate= 0.725 % Test Success Rate= 0.770513 %
CLASS(10) REP(10) Training Iteration: 5 Learning Success Rate= 0.798507 % Cross Validation Success Rate= 0.758333 % Test Success Rate= 0.730769 %
CLASS(10) REP(10) Training Iteration: 6 Learning Success Rate= 0.789552 % Cross Validation Success Rate= 0.708333 % Test Success Rate= 0.764103 %
CLASS(10) REP(10) Training Iteration: 7 Learning Success Rate= 0.809702 % Cross Validation Success Rate= 0.758333 % Test Success Rate= 0.758974 %
CLASS(10) REP(10) Training Iteration: 8 Learning Success Rate= 0.835075 % Cross Validation Success Rate= 0.75 % Test Success Rate= 0.792308 %
CLASS(10) REP(10) Training Iteration: 9 Learning Success Rate= 0.841791 % Cross Validation Success Rate= 0.8 % Test Success Rate= 0.757692 %
CLASS(10) REP(10) Training Iteration: 10 Learning Success Rate= 0.838806 % Cross Validation Success Rate= 0.666667 % Test Success Rate= 0.767949 %
CLASS(10) REP(10) Training Iteration: 11 Learning Success Rate= 0.826119 % Cross Validation Success Rate= 0.733333 % Test Success Rate= 0.770513 %
CLASS(10) REP(10) Training Iteration: 12 Learning Success Rate= 0.84403 % Cross Validation Success Rate= 0.766667 % Test Success Rate= 0.762821 %
CLASS(10) REP(10) Training Iteration: 13 Learning Success Rate= 0.851493 % Cross Validation Success Rate= 0.816667 % Test Success Rate= 0.784615 %
CLASS(10) REP(10) Training Iteration: 14 Learning Success Rate= 0.876866 % Cross Validation Success Rate= 0.808333 % Test Success Rate= 0.79359 %
CLASS(10) REP(10) Training Iteration: 15 Learning Success Rate= 0.852239 % Cross Validation Success Rate= 0.783333 % Test Success Rate= 0.797436 %
CLASS(10) REP(10) Training Iteration: 16 Learning Success Rate= 0.870149 % Cross Validation Success Rate= 0.75 % Test Success Rate= 0.79359 %
CLASS(10) REP(10) Training Iteration: 17 Learning Success Rate= 0.868657 % Cross Validation Success Rate= 0.825 % Test Success Rate= 0.805128 %
CLASS(10) REP(10) Training Iteration: 18 Learning Success Rate= 0.871642 % Cross Validation Success Rate= 0.866667 % Test Success Rate= 0.775641 %
CLASS(10) REP(10) Training Iteration: 19 Learning Success Rate= 0.85 % Cross Validation Success Rate= 0.85 % Test Success Rate= 0.796154 %
