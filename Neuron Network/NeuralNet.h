#pragma once
#include "file_access.h"

/************************************************************************/
/******************************Global Variables**************************/
/************************************************************************/

static int   NUMBER_OF_OUTPUT		=   10;									//To be modified if needed
extern double DEFAULT_WEIGHT_VALUES;
extern double LOWER_RANDOM_BOUND;
extern double UPPER_RANDOM_BOUND;
extern int	  ULBOUNDPRECISION;
extern int    TRAIN_A_NETWORK;
extern float  LEARNING_VALUE_N;
extern int    NUMBEROFDATAPERSAMPLE;
extern int    SIZE_OF_NETWORK;
extern std::string CONFIG_PATH;			
extern std::string LOAD_WEIGHTS_FROM_PATH;
extern std::string OUTPUT_LOGFILE;
extern std::string TRAIN_DB_NAME;
extern std::string VC_DB_NAME;
extern std::string TEST_DB_NAME;
extern int FIRST_LAYER_SIZE;
extern constants::FILTERING_OPTION  FILTERING_OPTION_CHOSEN;
extern constants::NUMBER_OF_SAMPLES_ACCEPTED NUMBER_OF_SAMPLE_PER_FILE;
extern int SUCCESS_RATE_THRESHOLD;
extern int TOTALRANDOMQTY;
extern int ADJUSTEDLOWERBOUND;
extern int LOAD_SAVED_WEIGHTS;
extern int NUMBER_OF_ITERATIONS;
extern int INITWEIGHTSMODE;


extern int		NUMBEROFREPSPERCLASS;
extern int		NUMBEROFCLASSES;
extern float	ALPHA;
extern int		ADJUSTEDLOWERBOUND;
extern int		TOTALRANDOMQTY;


/************************************************************************/
/******************************Data Structures***************************/
/************************************************************************/

typedef struct {
	int iSizeOfLayer;
	int ySizeOfLayer;
} WeightBinFile;

typedef enum										//Enumeration for classifying Weights Initialization
{
	DEFAULT,
	RANDOM,
	FILE_MODE
}INIT_WEIGHTS_FROM;

extern INIT_WEIGHTS_FROM DEFAULT_WEIGHT_MODE;

typedef enum										//Enumeration for classifying Activation Functions
{
	BENTIDENTITY,
	GAUSSIAN,
	SIGMOID,
	TANH
}ACTIVATION_FUNCTION;

extern ACTIVATION_FUNCTION ACTIVATIONFUNCTION;

class Neuron_Network;								//Call Forward for class SavedFileSettings Circular Dependencies


void ShowIntroductionAndPause();

/*This structure will be used to load and unload all saved settings from a file*/
class SavedFileSettings
{
public:

	/*-----------------------------------------------------------------*/
	//Function Description    : This function create a configuration 
	//	file if ever it gets erased or becomes unsable.							
	/*-----------------------------------------------------------------*/
	int GenerateConfigFile();

	/*-----------------------------------------------------------------*/
	//Function Parameters:	This function is used to read configuration
	//	parameters from the .config file. It does not load the global
	//	variables however.
	/*-----------------------------------------------------------------*/
	int ReadFromConfigFile();

	/*-----------------------------------------------------------------*/
	//Function Parameters:	Once ReadFromConfigFile() is executed
	//	this function can now be called, charging up all global vars.
	/*-----------------------------------------------------------------*/
	void LoadGlobalVariablesWithMyConfiguartion();
		
	/*-----------------------------------------------------------------*/
	//Function Parameters:	This function is used to save the weights
	//	and settings of the currentweights 3D vector. This function
	//	is adapted to work for any 3D vector size.
	/*-----------------------------------------------------------------*/
	//void WriteCurrentWeightsToBin(std::string MyPath, Neuron_Network& NNetwork);
	void WriteCurrentWeightsToBin(std::string MyPath, Neuron_Network& NNetwork);

	/*-----------------------------------------------------------------*/
	//Function Parameters:	This function is very important for loading
	//	up saved weights form previous training events. At this point
	//	one may use this to load up a network with high efficiency,
	//	without having to retrain it.
	/*-----------------------------------------------------------------*/
	void ReadCurrentWeightsFromBin(std::string MyPath, Neuron_Network& NNetwork);



	/*-----------------------------------------------------------------*/
	/*-----------------------Config File Variables---------------------*/
	/*-----------------------------------------------------------------*/
	ACTIVATION_FUNCTION ChosenActivationFunction;
	int SizeOfNeuralNetworkChosen;
	float LearningValue_N;
	INIT_WEIGHTS_FROM InitWeightsModes;
	float InitWeightsValues;
	float LowerBoundRandomValue;
	float UpperBoundRandomValue;
	int RandomValueStep;
	int NumberOfDataKeptPerSample;
	std::string LoadWeightsStructureFromPath;

	std::string TrainDataBaseBinFile;
	std::string VCDataBaseBinFile;
	std::string TestDBBinFile;
	int NumberOfNeuronsForFirstLayer;
	constants::FILTERING_OPTION FilteringOption;
	constants::NUMBER_OF_SAMPLES_ACCEPTED NumberOfSamplePerFile;
	int SuccessRateThreshold;
	int NumberOfIterations;
	int NumberOfRepsPerClass;
	int NumberOfClasses;
	float Alpha;
	int InitWeightsMode;



};

class Neuron
{
public:

	/*-----------------------------------------------------------------*/
	//Function Description:	A simple function that tells phase what 
	//	activation function to use.
	/*-----------------------------------------------------------------*/
	void UseActivationFunction(ACTIVATION_FUNCTION ChosenFunction, float i_node);

	/*-----------------------------------------------------------------*/
	//Function Parameters:	Same as UseActivationFunction() but for 
	//	derived activation function.
	/*-----------------------------------------------------------------*/
	float UseDerivativeActivationFunction(ACTIVATION_FUNCTION ChosenFunction, float i_node);

	std::vector <float> inputs;
	float i_node;
	float output;
	float bias;
	float error;
	float error_sum;
};


class Neuron_Network											
{
public:

	/*-----------------------------------------------------------------*/
	//Function Description:	This Function is critical to the creationg
	//	of a new Neural Network. It sets the number of layers. This
	//	function needs to be called before SetLayerSize().
	/*-----------------------------------------------------------------*/
	int SetNetworkLayers(int NumberLayers);

	/*-----------------------------------------------------------------*/
	//Function Description:	Being another critical function, this one
	//	sets the number of neurons for a specific layer.
	/*-----------------------------------------------------------------*/
	int SetLayerSize(int LayerNumber, int NumberNeurons);

	/*-----------------------------------------------------------------*/
	//Function Description:	This function is used to set the number of
	//	inputs to the neural network.
	/*-----------------------------------------------------------------*/
	int SetInputSize(int InputNumber);

	/*-----------------------------------------------------------------*/
	//Function Description:	For this particular project, this function
	//	is only used for debugging purposes.
	/*-----------------------------------------------------------------*/
	int SetInputTestValues();

	/*-----------------------------------------------------------------*/
	//Function Description:	This function should only be called when 
	//	the neural network is sized and ready for use; otherwise, it will
	//	cause overflow of data in certain vectors.
	/*-----------------------------------------------------------------*/
	int CreateNetworkWeights();

	/*-----------------------------------------------------------------*/
	//Function Description:	This will fill the weights vector values
	//	according to the mode you give it. Check the param struct
	//	for details.
	/*-----------------------------------------------------------------*/
	int InitWeights(INIT_WEIGHTS_FROM);

	/*-----------------------------------------------------------------*/
	//Function Description:	Very essential after every iteration during
	//	training a network. Resets buffer data after one iteration.
	/*-----------------------------------------------------------------*/
	void ClearAllTempData();

	/*-----------------------------------------------------------------*/
	//Function Description:	This function resizes the input layer according	
	//	to the size of the Data Set produced from the database.
	//	This function needs to be called once before TrainNetwork is used.
	//  IE:  NNetwork.LoadInputsFromDataBase(TrainingDataBase, 0);
	/*-----------------------------------------------------------------*/
	int LoadInputsFromDataBase(std::vector<File_Class>& db_structure, int Current_File_number);

	/*-----------------------------------------------------------------*/
	//Function Description:	This function is used to run the first phase
	//	of Training a neural Network.						(Phase 1)
	/*-----------------------------------------------------------------*/
	int UpdateAllNetworkActivationFunctions(ACTIVATION_FUNCTION ActivationFunction);

	/*-----------------------------------------------------------------*/
	//Function Description:	This function is used during TrainNetwork.
	//	It is used in conjuction with CheckForErrors() to fetch a wanted
	//	output and compare with the one we got during Phase1.
	/*-----------------------------------------------------------------*/
	int SetWantedOutput(std::vector<File_Class>& db_structure, int Current_File_number, int Number_of_Output);

	/*-----------------------------------------------------------------*/
	//Function Description:	This function basically checks for errors
	//	and returns 0 if no errors. Also increments a counter.
	/*-----------------------------------------------------------------*/
	float CheckForErrors(std::vector<File_Class>& db_structure, int Current_File_number);

	/*-----------------------------------------------------------------*/
	//Function Description:	This function needs to be used after 
	//	UpdateAllNetworkActivationFunctions().				(Phase 2)
	/*-----------------------------------------------------------------*/
	int UpdateAllSignalErrors(ACTIVATION_FUNCTION ActivationFunction);				

	/*-----------------------------------------------------------------*/
	//Function Description:	This function is called after 
	//	UpdateAllSignalErrors().							(Phase 3)
	/*-----------------------------------------------------------------*/
	int CalculateNewWeights(float learningValue);															

	/*-----------------------------------------------------------------*/
	//Function Description:	This function needs to be called after 
	//	CalculateNewWeights() to refresh all weights.		(Phase 4)
	/*-----------------------------------------------------------------*/
	int UpdateAllNewWeights();																

	/*-----------------------------------------------------------------*/
	//Function Description:	Main function that contains all 4 phases.
	//	also keeps count of the success rate for the given training network.
	/*-----------------------------------------------------------------*/
	int TrainNetwork(std::vector<File_Class>& db_structure, SavedFileSettings MyConfig);

	/*-----------------------------------------------------------------*/
	//Function Description:	This function is used after the training.
	//	In order to properly validate the trained network.
	/*-----------------------------------------------------------------*/
	int TestNetwork(std::vector<File_Class>& db_structure, SavedFileSettings MyConfig);


	/*-----------------------------------------------------------------*/
	/*-----------------------Neural Network Parameters-----------------*/
	/*-----------------------------------------------------------------*/
	void WriteOutputValidation(std::string MyPath, std::vector<File_Class>& db_structure, int IterationNumber, int iterationNumbera);
	int NumberOfTrainingSucceededOutput;
	int NumberOfCrossValidationSucceededOutput;
	int NumberOfTestSucceededOutput;


	int NetworkLayerNumber;
	std::vector <Neuron> inputLayer;
	std::vector <Neuron> firstLayer;
	std::vector <std::vector <Neuron>> hiddenLayers;
	std::vector <Neuron> outputLayer;
	std::vector <float> WantedOutput;
	std::vector< std::vector< std::vector<float>>> currentweights;
	std::vector< std::vector< std::vector<float>>> deltaweights;
	std::vector< std::vector< std::vector<float>>> previousweights;
};