_____________________________________________________________________


Started Training Network with DataBase_train_B_40
Configuration: 
                     Activation Function : 2
                     Weight Values Mode  : 1
                     Default Values		 : 0.1
                     Network Size        : 3
                     Number of elements/s: 12
                     Learning Value (N)  : 0.6
                     Rand LWR Value      : -0.1
                     Rand UPPER Value    : 0.1
                     Output Layer Size   : 10
 (Check config file for details on params)

L1(40) L2(25) OUT(10) Training Iteration: 0 Learning Success Rate= 19.6269 % Cross Validation Success Rate= 39.1667 % Test Success Rate= 38.2051 %
L1(40) L2(25) OUT(10) Training Iteration: 1 Learning Success Rate= 44.4776 % Cross Validation Success Rate= 53.3333 % Test Success Rate= 49.7436 %
L1(40) L2(25) OUT(10) Training Iteration: 2 Learning Success Rate= 59.9254 % Cross Validation Success Rate= 58.3333 % Test Success Rate= 57.4359 %
L1(40) L2(25) OUT(10) Training Iteration: 3 Learning Success Rate= 60.8209 % Cross Validation Success Rate= 45.8333 % Test Success Rate= 59.1026 %
L1(40) L2(25) OUT(10) Training Iteration: 4 Learning Success Rate= 62.4627 % Cross Validation Success Rate= 55 % Test Success Rate= 60.5128 %
L1(40) L2(25) OUT(10) Training Iteration: 5 Learning Success Rate= 62.6866 % Cross Validation Success Rate= 59.1667 % Test Success Rate= 61.1538 %
L1(40) L2(25) OUT(10) Training Iteration: 6 Learning Success Rate= 65.2239 % Cross Validation Success Rate= 58.3333 % Test Success Rate= 62.4359 %
L1(40) L2(25) OUT(10) Training Iteration: 7 Learning Success Rate= 69.1045 % Cross Validation Success Rate= 61.6667 % Test Success Rate= 58.0769 %
L1(40) L2(25) OUT(10) Training Iteration: 8 Learning Success Rate= 67.9851 % Cross Validation Success Rate= 60.8333 % Test Success Rate= 59.4872 %
L1(40) L2(25) OUT(10) Training Iteration: 9 Learning Success Rate= 67.4627 % Cross Validation Success Rate= 61.6667 % Test Success Rate= 60.1282 %
L1(40) L2(25) OUT(10) Training Iteration: 10 Learning Success Rate= 67.8358 % Cross Validation Success Rate= 60.8333 % Test Success Rate= 65.7692 %
L1(40) L2(25) OUT(10) Training Iteration: 11 Learning Success Rate= 70.9701 % Cross Validation Success Rate= 58.3333 % Test Success Rate= 60.2564 %
L1(40) L2(25) OUT(10) Training Iteration: 12 Learning Success Rate= 71.4925 % Cross Validation Success Rate= 73.3333 % Test Success Rate= 67.6923 %
L1(40) L2(25) OUT(10) Training Iteration: 13 Learning Success Rate= 74.2537 % Cross Validation Success Rate= 67.5 % Test Success Rate= 67.4359 %
L1(40) L2(25) OUT(10) Training Iteration: 14 Learning Success Rate= 71.3433 % Cross Validation Success Rate= 62.5 % Test Success Rate= 66.9231 %
L1(40) L2(25) OUT(10) Training Iteration: 15 Learning Success Rate= 75.6716 % Cross Validation Success Rate= 64.1667 % Test Success Rate= 65.3846 %
L1(40) L2(25) OUT(10) Training Iteration: 16 Learning Success Rate= 76.791 % Cross Validation Success Rate= 68.3333 % Test Success Rate= 66.9231 %
L1(40) L2(25) OUT(10) Training Iteration: 17 Learning Success Rate= 76.194 % Cross Validation Success Rate= 63.3333 % Test Success Rate= 64.6154 %
L1(40) L2(25) OUT(10) Training Iteration: 18 Learning Success Rate= 76.4925 % Cross Validation Success Rate= 67.5 % Test Success Rate= 68.8462 %
L1(40) L2(25) OUT(10) Training Iteration: 19 Learning Success Rate= 76.194 % Cross Validation Success Rate= 64.1667 % Test Success Rate= 67.9487 %
L1(40) L2(25) OUT(10) Training Iteration: 20 Learning Success Rate= 77.7612 % Cross Validation Success Rate= 67.5 % Test Success Rate= 65.2564 %
L1(40) L2(25) OUT(10) Training Iteration: 21 Learning Success Rate= 76.2687 % Cross Validation Success Rate= 65.8333 % Test Success Rate= 69.359 %
L1(40) L2(25) OUT(10) Training Iteration: 22 Learning Success Rate= 80.3731 % Cross Validation Success Rate= 70 % Test Success Rate= 71.0256 %
L1(40) L2(25) OUT(10) Training Iteration: 23 Learning Success Rate= 79.8507 % Cross Validation Success Rate= 75 % Test Success Rate= 71.5385 %
L1(40) L2(25) OUT(10) Training Iteration: 24 Learning Success Rate= 81.0448 % Cross Validation Success Rate= 71.6667 % Test Success Rate= 75.3846 %
L1(40) L2(25) OUT(10) Training Iteration: 25 Learning Success Rate= 78.8806 % Cross Validation Success Rate= 65 % Test Success Rate= 68.9744 %
L1(40) L2(25) OUT(10) Training Iteration: 26 Learning Success Rate= 79.5522 % Cross Validation Success Rate= 69.1667 % Test Success Rate= 70 %
L1(40) L2(25) OUT(10) Training Iteration: 27 Learning Success Rate= 79.7015 % Cross Validation Success Rate= 72.5 % Test Success Rate= 66.7949 %
L1(40) L2(25) OUT(10) Training Iteration: 28 Learning Success Rate= 79.6269 % Cross Validation Success Rate= 63.3333 % Test Success Rate= 69.4872 %
L1(40) L2(25) OUT(10) Training Iteration: 29 Learning Success Rate= 80.8955 % Cross Validation Success Rate= 70.8333 % Test Success Rate= 71.5385 %
L1(40) L2(25) OUT(10) Training Iteration: 30 Learning Success Rate= 83.806 % Cross Validation Success Rate= 69.1667 % Test Success Rate= 71.7949 %
L1(40) L2(25) OUT(10) Training Iteration: 31 Learning Success Rate= 84.1045 % Cross Validation Success Rate= 75.8333 % Test Success Rate= 74.359 %
L1(40) L2(25) OUT(10) Training Iteration: 32 Learning Success Rate= 82.7612 % Cross Validation Success Rate= 71.6667 % Test Success Rate= 72.1795 %
L1(40) L2(25) OUT(10) Training Iteration: 33 Learning Success Rate= 83.806 % Cross Validation Success Rate= 65 % Test Success Rate= 70 %
L1(40) L2(25) OUT(10) Training Iteration: 34 Learning Success Rate= 82.0149 % Cross Validation Success Rate= 67.5 % Test Success Rate= 71.5385 %
L1(40) L2(25) OUT(10) Training Iteration: 35 Learning Success Rate= 84.3284 % Cross Validation Success Rate= 66.6667 % Test Success Rate= 73.2051 %
L1(40) L2(25) OUT(10) Training Iteration: 36 Learning Success Rate= 85.7463 % Cross Validation Success Rate= 74.1667 % Test Success Rate= 72.9487 %
L1(40) L2(25) OUT(10) Training Iteration: 37 Learning Success Rate= 86.3433 % Cross Validation Success Rate= 70 % Test Success Rate= 74.1026 %
L1(40) L2(25) OUT(10) Training Iteration: 38 Learning Success Rate= 86.194 % Cross Validation Success Rate= 72.5 % Test Success Rate= 73.9744 %
L1(40) L2(25) OUT(10) Training Iteration: 39 Learning Success Rate= 86.0448 % Cross Validation Success Rate= 75.8333 % Test Success Rate= 73.7179 %
L1(40) L2(25) OUT(10) Training Iteration: 40 Learning Success Rate= 86.4179 % Cross Validation Success Rate= 78.3333 % Test Success Rate= 73.9744 %
L1(40) L2(25) OUT(10) Training Iteration: 41 Learning Success Rate= 83.9552 % Cross Validation Success Rate= 71.6667 % Test Success Rate= 72.6923 %
L1(40) L2(25) OUT(10) Training Iteration: 42 Learning Success Rate= 83.9552 % Cross Validation Success Rate= 73.3333 % Test Success Rate= 72.8205 %
L1(40) L2(25) OUT(10) Training Iteration: 43 Learning Success Rate= 87.0149 % Cross Validation Success Rate= 77.5 % Test Success Rate= 73.5897 %
L1(40) L2(25) OUT(10) Training Iteration: 44 Learning Success Rate= 87.0896 % Cross Validation Success Rate= 74.1667 % Test Success Rate= 76.5385 %
L1(40) L2(25) OUT(10) Training Iteration: 45 Learning Success Rate= 89.6269 % Cross Validation Success Rate= 75 % Test Success Rate= 75.641 %
L1(40) L2(25) OUT(10) Training Iteration: 46 Learning Success Rate= 87.6119 % Cross Validation Success Rate= 74.1667 % Test Success Rate= 72.8205 %
L1(40) L2(25) OUT(10) Training Iteration: 47 Learning Success Rate= 87.6866 % Cross Validation Success Rate= 72.5 % Test Success Rate= 76.2821 %
L1(40) L2(25) OUT(10) Training Iteration: 48 Learning Success Rate= 87.7612 % Cross Validation Success Rate= 72.5 % Test Success Rate= 75 %
L1(40) L2(25) OUT(10) Training Iteration: 49 Learning Success Rate= 87.0149 % Cross Validation Success Rate= 68.3333 % Test Success Rate= 72.1795 %
L1(40) L2(25) OUT(10) Training Iteration: 50 Learning Success Rate= 87.9851 % Cross Validation Success Rate= 72.5 % Test Success Rate= 77.9487 %
L1(40) L2(25) OUT(10) Training Iteration: 51 Learning Success Rate= 88.2836 % Cross Validation Success Rate= 74.1667 % Test Success Rate= 74.4872 %
L1(40) L2(25) OUT(10) Training Iteration: 52 Learning Success Rate= 89.3284 % Cross Validation Success Rate= 68.3333 % Test Success Rate= 75.1282 %
L1(40) L2(25) OUT(10) Training Iteration: 53 Learning Success Rate= 90.4478 % Cross Validation Success Rate= 70.8333 % Test Success Rate= 77.3077 %
L1(40) L2(25) OUT(10) Training Iteration: 54 Learning Success Rate= 88.0597 % Cross Validation Success Rate= 75.8333 % Test Success Rate= 76.7949 %
L1(40) L2(25) OUT(10) Training Iteration: 55 Learning Success Rate= 88.1343 % Cross Validation Success Rate= 70.8333 % Test Success Rate= 75.7692 %
L1(40) L2(25) OUT(10) Training Iteration: 56 Learning Success Rate= 87.7612 % Cross Validation Success Rate= 74.1667 % Test Success Rate= 75.7692 %
L1(40) L2(25) OUT(10) Training Iteration: 57 Learning Success Rate= 88.209 % Cross Validation Success Rate= 73.3333 % Test Success Rate= 77.1795 %
L1(40) L2(25) OUT(10) Training Iteration: 58 Learning Success Rate= 89.403 % Cross Validation Success Rate= 72.5 % Test Success Rate= 75.8974 %
L1(40) L2(25) OUT(10) Training Iteration: 59 Learning Success Rate= 87.6866 % Cross Validation Success Rate= 75.8333 % Test Success Rate= 75.1282 %
L1(40) L2(25) OUT(10) Training Iteration: 60 Learning Success Rate= 88.9552 % Cross Validation Success Rate= 75 % Test Success Rate= 73.0769 %
L1(40) L2(25) OUT(10) Training Iteration: 61 Learning Success Rate= 85.8955 % Cross Validation Success Rate= 75.8333 % Test Success Rate= 72.8205 %
L1(40) L2(25) OUT(10) Training Iteration: 62 Learning Success Rate= 89.1045 % Cross Validation Success Rate= 75.8333 % Test Success Rate= 72.4359 %
L1(40) L2(25) OUT(10) Training Iteration: 63 Learning Success Rate= 87.0149 % Cross Validation Success Rate= 69.1667 % Test Success Rate= 75.2564 %
L1(40) L2(25) OUT(10) Training Iteration: 64 Learning Success Rate= 87.4627 % Cross Validation Success Rate= 73.3333 % Test Success Rate= 73.8462 %
L1(40) L2(25) OUT(10) Training Iteration: 65 Learning Success Rate= 89.9254 % Cross Validation Success Rate= 76.6667 % Test Success Rate= 73.2051 %
L1(40) L2(25) OUT(10) Training Iteration: 66 Learning Success Rate= 88.1343 % Cross Validation Success Rate= 72.5 % Test Success Rate= 72.8205 %
L1(40) L2(25) OUT(10) Training Iteration: 67 Learning Success Rate= 91.194 % Cross Validation Success Rate= 75 % Test Success Rate= 75.2564 %
L1(40) L2(25) OUT(10) Training Iteration: 68 Learning Success Rate= 90.0746 % Cross Validation Success Rate= 72.5 % Test Success Rate= 75.3846 %
L1(40) L2(25) OUT(10) Training Iteration: 69 Learning Success Rate= 88.7313 % Cross Validation Success Rate= 65 % Test Success Rate= 72.9487 %
L1(40) L2(25) OUT(10) Training Iteration: 70 Learning Success Rate= 89.2537 % Cross Validation Success Rate= 75.8333 % Test Success Rate= 74.7436 %
L1(40) L2(25) OUT(10) Training Iteration: 71 Learning Success Rate= 90.597 % Cross Validation Success Rate= 70.8333 % Test Success Rate= 75.7692 %
L1(40) L2(25) OUT(10) Training Iteration: 72 Learning Success Rate= 91.8657 % Cross Validation Success Rate= 75.8333 % Test Success Rate= 74.6154 %
L1(40) L2(25) OUT(10) Training Iteration: 73 Learning Success Rate= 90.6716 % Cross Validation Success Rate= 76.6667 % Test Success Rate= 73.4615 %
L1(40) L2(25) OUT(10) Training Iteration: 74 Learning Success Rate= 90.8955 % Cross Validation Success Rate= 73.3333 % Test Success Rate= 75.8974 %
L1(40) L2(25) OUT(10) Training Iteration: 75 Learning Success Rate= 90.7463 % Cross Validation Success Rate= 75 % Test Success Rate= 76.1538 %
L1(40) L2(25) OUT(10) Training Iteration: 76 Learning Success Rate= 90.8209 % Cross Validation Success Rate= 75 % Test Success Rate= 75.1282 %
L1(40) L2(25) OUT(10) Training Iteration: 77 Learning Success Rate= 89.403 % Cross Validation Success Rate= 75 % Test Success Rate= 77.4359 %
L1(40) L2(25) OUT(10) Training Iteration: 78 Learning Success Rate= 92.6119 % Cross Validation Success Rate= 78.3333 % Test Success Rate= 77.8205 %
L1(40) L2(25) OUT(10) Training Iteration: 79 Learning Success Rate= 91.4179 % Cross Validation Success Rate= 73.3333 % Test Success Rate= 75.7692 %
L1(40) L2(25) OUT(10) Training Iteration: 80 Learning Success Rate= 94.1791 % Cross Validation Success Rate= 74.1667 % Test Success Rate= 77.4359 %
L1(40) L2(25) OUT(10) Training Iteration: 81 Learning Success Rate= 93.7313 % Cross Validation Success Rate= 75 % Test Success Rate= 75.7692 %
L1(40) L2(25) OUT(10) Training Iteration: 82 Learning Success Rate= 92.3881 % Cross Validation Success Rate= 71.6667 % Test Success Rate= 76.2821 %
L1(40) L2(25) OUT(10) Training Iteration: 83 Learning Success Rate= 91.9403 % Cross Validation Success Rate= 70.8333 % Test Success Rate= 75.1282 %
L1(40) L2(25) OUT(10) Training Iteration: 84 Learning Success Rate= 91.7164 % Cross Validation Success Rate= 72.5 % Test Success Rate= 73.9744 %
L1(40) L2(25) OUT(10) Training Iteration: 85 Learning Success Rate= 90.8955 % Cross Validation Success Rate= 70.8333 % Test Success Rate= 77.8205 %
L1(40) L2(25) OUT(10) Training Iteration: 86 Learning Success Rate= 90.9701 % Cross Validation Success Rate= 70.8333 % Test Success Rate= 77.5641 %
L1(40) L2(25) OUT(10) Training Iteration: 87 Learning Success Rate= 91.194 % Cross Validation Success Rate= 77.5 % Test Success Rate= 76.6667 %
L1(40) L2(25) OUT(10) Training Iteration: 88 Learning Success Rate= 91.9403 % Cross Validation Success Rate= 68.3333 % Test Success Rate= 75.3846 %
L1(40) L2(25) OUT(10) Training Iteration: 89 Learning Success Rate= 93.2836 % Cross Validation Success Rate= 70.8333 % Test Success Rate= 77.1795 %
L1(40) L2(25) OUT(10) Training Iteration: 90 Learning Success Rate= 91.194 % Cross Validation Success Rate= 71.6667 % Test Success Rate= 74.4872 %
L1(40) L2(25) OUT(10) Training Iteration: 91 Learning Success Rate= 91.4925 % Cross Validation Success Rate= 71.6667 % Test Success Rate= 75.641 %
L1(40) L2(25) OUT(10) Training Iteration: 92 Learning Success Rate= 89.4776 % Cross Validation Success Rate= 74.1667 % Test Success Rate= 74.4872 %
L1(40) L2(25) OUT(10) Training Iteration: 93 Learning Success Rate= 88.9552 % Cross Validation Success Rate= 73.3333 % Test Success Rate= 76.6667 %
L1(40) L2(25) OUT(10) Training Iteration: 94 Learning Success Rate= 88.3582 % Cross Validation Success Rate= 74.1667 % Test Success Rate= 73.7179 %
L1(40) L2(25) OUT(10) Training Iteration: 95 Learning Success Rate= 90.597 % Cross Validation Success Rate= 77.5 % Test Success Rate= 74.2308 %
L1(40) L2(25) OUT(10) Training Iteration: 96 Learning Success Rate= 92.3881 % Cross Validation Success Rate= 75.8333 % Test Success Rate= 74.8718 %
L1(40) L2(25) OUT(10) Training Iteration: 97 Learning Success Rate= 91.9403 % Cross Validation Success Rate= 71.6667 % Test Success Rate= 75.2564 %
L1(40) L2(25) OUT(10) Training Iteration: 98 Learning Success Rate= 92.2388 % Cross Validation Success Rate= 70.8333 % Test Success Rate= 75.5128 %
L1(40) L2(25) OUT(10) Training Iteration: 99 Learning Success Rate= 91.6418 % Cross Validation Success Rate= 69.1667 % Test Success Rate= 74.2308 %
