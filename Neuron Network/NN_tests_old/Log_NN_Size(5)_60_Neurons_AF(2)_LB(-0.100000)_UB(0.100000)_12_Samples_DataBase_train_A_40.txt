_____________________________________________________________________


Started Training Network with DataBase_train_A_40
Configuration: 
                     Activation Function : 2
                     Weight Values Mode  : 1
                     Default Values		 : 0.1
                     Network Size        : 5
                     Number of elements/s: 12
                     Learning Value (N)  : 0.1
                     Rand LWR Value      : -0.1
                     Rand UPPER Value    : 0.1
                     Output Layer Size   : 10
 (Check config file for details on params)

L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 0 Learning Success Rate= 10.7463 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
_____________________________________________________________________


Started Training Network with DataBase_train_A_40
Configuration: 
                     Activation Function : 2
                     Weight Values Mode  : 1
                     Default Values		 : 0.1
                     Network Size        : 4
                     Number of elements/s: 12
                     Learning Value (N)  : 0.1
                     Rand LWR Value      : -0.1
                     Rand UPPER Value    : 0.1
                     Output Layer Size   : 10
 (Check config file for details on params)

L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 1 Learning Success Rate= 8.8806 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 2 Learning Success Rate= 10.4478 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 3 Learning Success Rate= 10.4478 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 4 Learning Success Rate= 9.85075 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 5 Learning Success Rate= 7.53731 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 6 Learning Success Rate= 9.10448 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 7 Learning Success Rate= 10.3731 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 8 Learning Success Rate= 8.8806 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 9 Learning Success Rate= 10.2239 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 10 Learning Success Rate= 9.02985 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 11 Learning Success Rate= 9.70149 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 12 Learning Success Rate= 10.8955 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 13 Learning Success Rate= 10.2985 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 14 Learning Success Rate= 10.2239 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 15 Learning Success Rate= 10.0746 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 16 Learning Success Rate= 10.2239 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 17 Learning Success Rate= 9.92537 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 18 Learning Success Rate= 9.55224 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 19 Learning Success Rate= 9.1791 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 20 Learning Success Rate= 10 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 21 Learning Success Rate= 9.47761 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 22 Learning Success Rate= 10.3731 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 23 Learning Success Rate= 9.47761 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 24 Learning Success Rate= 9.62687 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 25 Learning Success Rate= 9.55224 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 26 Learning Success Rate= 10.2985 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 27 Learning Success Rate= 9.25373 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 28 Learning Success Rate= 11.2687 % Cross Validation Success Rate= 12.5 % Test Success Rate= 12.1795 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 29 Learning Success Rate= 10.7463 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 30 Learning Success Rate= 10.2985 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 31 Learning Success Rate= 11.0448 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 32 Learning Success Rate= 10.1493 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 33 Learning Success Rate= 8.95522 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 34 Learning Success Rate= 10.1493 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 35 Learning Success Rate= 9.32836 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 36 Learning Success Rate= 10.6716 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 37 Learning Success Rate= 9.85075 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 38 Learning Success Rate= 10.7463 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 39 Learning Success Rate= 12.3881 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 40 Learning Success Rate= 11.1194 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 41 Learning Success Rate= 9.47761 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 42 Learning Success Rate= 11.8657 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 43 Learning Success Rate= 10.4478 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 44 Learning Success Rate= 9.55224 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 45 Learning Success Rate= 10.5224 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 46 Learning Success Rate= 10.2239 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 47 Learning Success Rate= 9.25373 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 48 Learning Success Rate= 10.0746 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 49 Learning Success Rate= 10.1493 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 50 Learning Success Rate= 9.32836 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 51 Learning Success Rate= 9.32836 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 52 Learning Success Rate= 9.32836 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 53 Learning Success Rate= 10.9701 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 54 Learning Success Rate= 9.40298 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 55 Learning Success Rate= 9.70149 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 56 Learning Success Rate= 11.4179 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 57 Learning Success Rate= 10 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 58 Learning Success Rate= 10.6716 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 59 Learning Success Rate= 10.6716 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 60 Learning Success Rate= 11.4925 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 61 Learning Success Rate= 10.7463 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 62 Learning Success Rate= 9.40298 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 63 Learning Success Rate= 11.791 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 64 Learning Success Rate= 10.5224 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 65 Learning Success Rate= 10.597 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 66 Learning Success Rate= 9.02985 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 67 Learning Success Rate= 10.5224 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 68 Learning Success Rate= 8.20895 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 69 Learning Success Rate= 9.40298 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 70 Learning Success Rate= 10.6716 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 71 Learning Success Rate= 9.32836 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 72 Learning Success Rate= 10 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 73 Learning Success Rate= 9.85075 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 74 Learning Success Rate= 10.9701 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 75 Learning Success Rate= 11.194 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 76 Learning Success Rate= 10.3731 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 77 Learning Success Rate= 10.0746 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 78 Learning Success Rate= 10.8209 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 79 Learning Success Rate= 9.92537 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 80 Learning Success Rate= 10.2985 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 81 Learning Success Rate= 10.597 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 82 Learning Success Rate= 10.2985 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 83 Learning Success Rate= 11.791 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 84 Learning Success Rate= 10.4478 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 85 Learning Success Rate= 11.194 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 86 Learning Success Rate= 9.85075 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 87 Learning Success Rate= 10.597 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 88 Learning Success Rate= 8.58209 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 89 Learning Success Rate= 9.10448 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 90 Learning Success Rate= 10.597 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 91 Learning Success Rate= 9.62687 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
L1(60) L2(35) L3(26) L4(21) OUT(10) Training Iteration: 92 Learning Success Rate= 10 % Cross Validation Success Rate= 10 % Test Success Rate= 10 %
