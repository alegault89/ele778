/************************************************************************/
/******************************Data Structures***************************/
/************************************************************************/
#include "file_access.h"

typedef struct{
	unsigned int NumberOfrepresentation;
	unsigned int ClassNumber;
	float		 ShortestDistance;
}ShortestDistanceID_DataType;

typedef struct {
	int numberofrepperclass;
	int numberofclasses;
	int sizeofinputvector;
} WeightBinFileLVQ;

class TYPE_LVQ
{
public:

	std::vector<float> InputVector;									//Input   of size DatabaseSize

	std::vector<std::vector<std::vector<float>>> VectorWeights;		//VectorWeights - 
																	//		Number of Times we can represent 1 class [0..1]
																	//		Class Number							 [0..9]
																	//		Distance or weight value				 [SameSizeAsInput]

	float Alpha;
	int CurrentWantedOutput;
	float CurrentDistance;
	ShortestDistanceID_DataType ShortestDistanceID;

	/*-----------------------------------------------------------------*/
	//Function Description    : Function that Migrates all current
	// weights to a bin file as backup. The weights can be fetched using 
	// WriteCurrentLVQWeightsToBin() for further use.
	/*-----------------------------------------------------------------*/
	void	WriteCurrentLVQWeightsToBin(std::string MyPath, int NumberOfRepresentationPerClass, int NumberOfClassifications, TYPE_LVQ& NNetwork);

	/*-----------------------------------------------------------------*/
	//Function Description    : Use this function to fetch previously 
	// saved weights. The Function will dynamically resize NNetwork
	// and all of it's subsequent data structures according to the 
	// saved data created by WriteCurrentLVQWeightsToBin().
	/*-----------------------------------------------------------------*/
	void	ReadCurrentLVQWeightsFromBin(std::string MyPath, int NumberOfRepresentationPerClass, int NumberOfClassifications, TYPE_LVQ& NNetwork);

	/*-----------------------------------------------------------------*/
	//Function Description    : Simple function that sets most Parameters
	// to a default value. Please look at the function definition for
	// more details.
	/*-----------------------------------------------------------------*/
	void	InitializeVariables(float AlphaValue);

	/*-----------------------------------------------------------------*/
	//Function Description    : Using db_structure as it's incoming data
	// it will load the inputs accorind to the index "Current_File_number"
	// that you send it.
	/*-----------------------------------------------------------------*/
	void 	LoadInputsToVector(std::vector<File_Class>& db_structure, int Current_File_number);

	/*-----------------------------------------------------------------*/
	//Function Description    : This function will initialize all weights
	// according to the mode "InitWeightsMode". Mode can be 0 - 1
	/*-----------------------------------------------------------------*/
	void	InitWeights(std::vector<File_Class>& db_structure, int NumberOfRepresentationPerClass, int NumberOfClassifications, int InitWeightsMode);

	/*-----------------------------------------------------------------*/
	//Function Description    : Critical function that applies Kohonen
	// principles. Please review SOM and/or LVQ Algorithms for more details.
	/*-----------------------------------------------------------------*/
	void 	FindShortestDistance();

	/*-----------------------------------------------------------------*/
	//Function Description    : Critical Function that adjusts ther closest
	// Weights to it's appropriate distance.
	/*-----------------------------------------------------------------*/
	float 	AdjustWeights();						//Returns how much the shortest weight changed
};