#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <cmath>
#include "NeuralNet.h"
#include "file_access.h"
#include "LVQ.h"


using namespace std;


void ShowIntroductionAndPause()
{
std:string UserAnswer;
	cout << "_____________________________Welcome to Neural Networks Project_____________________________" << endl << endl <<
	    "	This program was build under the course instructions of ELE778.					 " << endl <<
	    "	In order to train a network, you must set the wanted parameters					 " << endl <<
	    "	in the corresponding .config file found in Neural Network repository.     		 " << endl <<
	    "	(Please leave one and only space between params and the value)  				 " << endl <<
	    "	Once this is done you may continue into this executable.						 " << endl <<
	    "	At this point, if you are sure of your parameters, then follow    				 " << endl <<
	    "	to the following steps and watch the 'magic' happen.  							 " << endl <<
	    "	Furthermore, if you would like to keep track of how well the    				 " << endl <<
	    "	training session went, I highly recommend looking up the output log. 			 " << endl <<
	    "	The name of the Output log is relative to the options you have chose    		 " << endl <<
	    "	during the last training or testing session found in the .config file. Enjoy...	 " << endl <<
	    "____________________________________________________________________________________________" << endl << endl;
	cout << "Press 'q' to quit and save after the current iteration" << endl<< endl;

	cout << "Load a saved network from file? "<< endl;
	cout << "'y' - To continue and load from .bin file" << endl;
	cout << "'n' - To continue to Training a Network" << endl;
	cout << "Type any other letter to exit" << endl;
	cout << "Hit Enter when you have made a selection" << endl << endl;
	cin >> UserAnswer;

	if(UserAnswer != "y" && UserAnswer != "Y" && UserAnswer != "n" && UserAnswer != "N")
		exit(EXIT_FAILURE);
	else 
	{
		if (UserAnswer == "y" || UserAnswer == "Y")
		{
			cout << endl << "Please make sure you have updated the Parameter 'LoadWeightsStructureFromPath' (without the extension)" << endl;
			cout << "and make sure that the DataStructure chosen has the same quantity of samples 40-50-60 and also has the same element/s " << endl;
			cout << "ei. 12 or 26, these Parameters are 'NumberOfSamplePerFile' & 'NumberOfDataKeptPerSample' in the configuration file." << endl;
			cout << "If you are ready and made sure you saved your settings, then follow the instruction bellow or click the 'x' button to your "<<"top right to exit the program..." << endl << endl;
			system("pause");

			cout << endl;
			
			LOAD_SAVED_WEIGHTS = 1;
		}
		else if (UserAnswer == "n" || UserAnswer == "N")
		{
			LOAD_SAVED_WEIGHTS = 0;
		}
	}
}

int SavedFileSettings::GenerateConfigFile()
{
		//Some Default Configurations
		//ChosenActivationFunction		= ACTIVATION_FUNCTION::SIGMOID;        					  	
		SizeOfNeuralNetworkChosen		= 3;                     
		//LearningValue_N					= 0.1 ;                				      		                  
		//InitWeightsModes				= INIT_WEIGHTS_FROM::RANDOM;               			                  
		//InitWeightsValues				= 0.1 ;                     				              
		LowerBoundRandomValue			= -0.1;                               
		UpperBoundRandomValue			= 0.1 ;                   			                   
		RandomValueStep					= 1000;                  					  	
		NumberOfDataKeptPerSample		= 12  ;   						  
		//LoadWeightsStructureFromPath	= "BackedUpWeights";               
		TrainDataBaseBinFile			= "DataBase_train";          
		VCDataBaseBinFile				= "DataBase_vc";                       
		TestDBBinFile					= "DataBase_test";                    
		//NumberOfNeuronsForFirstLayer	= 100;					      
		FilteringOption					= constants::FILTERING_OPTION::OPTIONB;						                    
		NumberOfSamplePerFile			= constants::NUMBER_OF_SAMPLES_ACCEPTED::NO_40_SAMPLES;   
		SuccessRateThreshold			= 99;
		NumberOfRepsPerClass			= 5;
		NumberOfClasses					= 10;
		Alpha							= 0.1;
		InitWeightsMode					= 1;
		NumberOfIterations				= 100;

	fstream ConfigFile(CONFIG_PATH, ios::out);

	if (ConfigFile.is_open())
	{

/*
		ConfigFile << "ChosenActivationFunction " << ChosenActivationFunction;
		ConfigFile.width(120);
		ConfigFile << right << "#Chose the function that will be used as activation per Neuron (0-BENTIDENTITY, 1-GAUSSIAN, 2-SIGMOID, 3-TANH)" << endl;
*/
		ConfigFile << "SizeOfNeuralNetworkChosen " << SizeOfNeuralNetworkChosen;
		ConfigFile.width(120);
		ConfigFile << right << "#Chose Number of Layers in Neural Network                                                                    " << endl;
/*
		ConfigFile << "LearningValue_N " << LearningValue_N;
		ConfigFile.width(120);
		ConfigFile << right << "#Chose The Learning Value N                                                                            " << endl;
*//*
		ConfigFile << "InitWeightsModes " << InitWeightsModes;
		ConfigFile.width(120);
		ConfigFile << right << "#Chose the default values for all weight (0-default, 1-random, 2-from bin)                             " << endl;
*//*
		ConfigFile << "InitWeightsValues " << InitWeightsValues;
		ConfigFile.width(120);
		ConfigFile << right << "#If Default chosen for previous parameter what values should you defualt them to?                 " << endl;
*/
		ConfigFile << "InitWeightsMode " << InitWeightsMode;
		ConfigFile.width(120);
		ConfigFile << right << "#Chose the default values for all weight (0-random, 1-from training data)                             " << endl;

		ConfigFile << "LowerBoundRandomValue " << LowerBoundRandomValue;
		ConfigFile.width(120);
		ConfigFile << right << "#If InitWeightsMode=0 then Chose Lowest Random value                                       " << endl;
	
		ConfigFile << "UpperBoundRandomValue " << UpperBoundRandomValue;
		ConfigFile.width(120);
		ConfigFile << right << "#If InitWeightsMode=0 then Chose Highest Random value                                       " << endl;

		ConfigFile << "RandomValueStep " << RandomValueStep;
		ConfigFile.width(170);
		ConfigFile << right << "#If InitWeightsMode=0 then this value will set the precision for the random generated values (values > 10e3 will cause RAND() to fail miserably)" << endl;

		ConfigFile << "NumberOfDataKeptPerSample " << NumberOfDataKeptPerSample;
		ConfigFile.width(120);
		ConfigFile << right << "#Used to define the size of your input layer inputlayersize = NumberofSamplePerTextFile x NumberOfDataKeptPerSample" << endl;

		/*ConfigFile << "TrainANetwork " << TrainANetwork;
		ConfigFile.width(120);
		ConfigFile << right << "#Binary value that determines if you want to train a network                          " << endl;*/
		/*ConfigFile << "LoadWeightsStructureFromPath " << LoadWeightsStructureFromPath;
		ConfigFile.width(120);
		ConfigFile << right << "#If InitWeightsValues=2 then this param will be used to load up a previously saved weight structure (*.bin only)" << endl;
*/
		/**/
		ConfigFile << "TrainDataBaseBinFile " << TrainDataBaseBinFile;
		ConfigFile.width(120);
		ConfigFile << right << "#The name of the Train Data set in a binary file (Please don't include the file extension)		  "      << endl;

		ConfigFile << "VCDataBaseBinFile " << VCDataBaseBinFile;
		ConfigFile.width(120);
		ConfigFile << right << "#The name of the CrossValidation Data set in a binary file (Please don't include the file extension)"     << endl;

		ConfigFile << "TestDBBinFile " << TestDBBinFile;
		ConfigFile.width(120);
		ConfigFile << right << "#The name of the Test Data set in a binary file (Please do not include the file extension)  	  "      << endl;
/*
		ConfigFile << "NumberOfNeuronsForFirstLayer " << NumberOfNeuronsForFirstLayer;
		ConfigFile.width(120);
		ConfigFile << right << "#Chose the number of Neurons of your first layer, the rest of the layers will be resized accordingly (by avg)"      << endl;
*/
		ConfigFile << "FilteringOption " << FilteringOption;
		ConfigFile.width(120);
		ConfigFile << right << "# 0-A(removing 0's intensity levels), 1-B(Keep highest Intensities), 2-C(average value threshold)"      << endl;

		ConfigFile << "NumberOfSamplePerFile " << NumberOfSamplePerFile;
		ConfigFile.width(120);
		ConfigFile << right << "# 40-(Filter and Keep 40 samples), 50-(Filter and Keep 50 samples), 60-(Filter and Keep 60 samples)"      << endl;

		ConfigFile << "SuccessRateThreshold " << SuccessRateThreshold;
		ConfigFile.width(120);
		ConfigFile << right << "# Traning success rate (in %) when the program will stop											"      << endl;

		
		ConfigFile << "NumberOfIterations " << NumberOfIterations;
		ConfigFile.width(120);
		ConfigFile << right << "# Change the number of times the training loop is performed											"	<< endl;
		
		ConfigFile << "NumberOfRepsPerClass " << NumberOfRepsPerClass;
		ConfigFile.width(120);
		ConfigFile << right << "# Change the number of representant for each class											" << endl;

		ConfigFile << "NumberOfClasses " << NumberOfClasses;
		ConfigFile.width(120);
		ConfigFile << right << "# Change the number of classes											" << endl;

		ConfigFile << "Alpha " << Alpha;
		ConfigFile.width(120);
		ConfigFile << right << "# Change the value of the alpha											" << endl;

		ConfigFile.close();
		return 1;
	}
	else
	{
		cout << __func__ << ": Cannot Write to file '" << CONFIG_PATH << "' is opened..." << endl;
		return 0;
	}
}

int SavedFileSettings::ReadFromConfigFile()
{
	int i;
	std::string CurrentLineFromFile;
	std::vector<string> EntireFileNotParsed;
	std::vector< std::vector<string> > ParsedFile;
	fstream ConfigFile(CONFIG_PATH, ios::in);
													


	if (ConfigFile.is_open())
	{
		while (getline(ConfigFile, CurrentLineFromFile))					//Fetch every line until EOF
			EntireFileNotParsed.push_back(CurrentLineFromFile);
		
	}
	else
	{
		cout << __func__ << ": Cannot Read from file '" << CONFIG_PATH << endl;
		return 0;
	}

	std::string token;																		//StringStream Variables
	std::stringstream ss("");
	ParsedFile.resize(EntireFileNotParsed.size());
	
	for (i = 0; i < EntireFileNotParsed.size(); i++)						//Parse the content
	{
		ss.clear();
		ss.str(EntireFileNotParsed[i]);

		for (int j = 0; j < 3; )
		{
			getline(ss, token, ' ');
			ParsedFile[i].push_back(token);
			j++;
		}
	}
	

	for (i = 0; i < ParsedFile.size(); i++)									//Store Data
	{
		if      (ParsedFile[i][0] == "ChosenActivationFunction"	)
			ChosenActivationFunction = (ACTIVATION_FUNCTION)atoi(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "SizeOfNeuralNetworkChosen")
			SizeOfNeuralNetworkChosen = atoi(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "LearningValue_N"			)
			LearningValue_N = (float)atof(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "InitWeightsModes"		)
			InitWeightsModes = (INIT_WEIGHTS_FROM)atoi(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "InitWeightsValues"		)
			InitWeightsValues = (float)atof(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "LowerBoundRandomValue"		)
			LowerBoundRandomValue = (float)atof(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "UpperBoundRandomValue"		)
			UpperBoundRandomValue = (float)atof(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "RandomValueStep"				)
			RandomValueStep = atoi(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "NumberOfDataKeptPerSample"	)
			NumberOfDataKeptPerSample = atoi(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "LoadWeightsStructureFromPath"	)
			LoadWeightsStructureFromPath = ParsedFile[i][1];

		else if (ParsedFile[i][0] == "TrainDataBaseBinFile"			)
			TrainDataBaseBinFile = ParsedFile[i][1];

		else if (ParsedFile[i][0] == "VCDataBaseBinFile"			)
			VCDataBaseBinFile = ParsedFile[i][1];

		else if (ParsedFile[i][0] == "TestDBBinFile"				)
			TestDBBinFile = ParsedFile[i][1];

		else if (ParsedFile[i][0] == "NumberOfNeuronsForFirstLayer"	)
			NumberOfNeuronsForFirstLayer = atoi(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "FilteringOption"				)
			FilteringOption = (constants::FILTERING_OPTION)atoi(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "NumberOfSamplePerFile"		)
			NumberOfSamplePerFile = (constants::NUMBER_OF_SAMPLES_ACCEPTED)atoi(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "SuccessRateThreshold"	)
			SuccessRateThreshold = atoi(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "NumberOfIterations"	)
			NumberOfIterations = atoi(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "NumberOfRepsPerClass")
			NumberOfRepsPerClass = atoi(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "NumberOfClasses")
			NumberOfClasses = atoi(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "Alpha")
			Alpha = (float)atof(ParsedFile[i][1].c_str());

		else if (ParsedFile[i][0] == "InitWeightsMode")
			InitWeightsMode = (float)atof(ParsedFile[i][1].c_str());
	}

	ConfigFile.close();
	return 1;
}

void SavedFileSettings::LoadGlobalVariablesWithMyConfiguartion()
{
	/*Load inputs to global vars*/
	//ACTIVATIONFUNCTION			= ChosenActivationFunction;	//not used in lab3
	//DEFAULT_WEIGHT_MODE			= InitWeightsModes;	//not used
	NUMBER_OF_ITERATIONS		= NumberOfIterations;	//used in lvqmain

	if(!LOAD_SAVED_WEIGHTS)		SIZE_OF_NETWORK			= SizeOfNeuralNetworkChosen; //not used
	
	//LEARNING_VALUE_N			= LearningValue_N;	//not used
	//DEFAULT_WEIGHT_VALUES		= InitWeightsValues;	//not used
	LOWER_RANDOM_BOUND			= LowerBoundRandomValue;	//used in lvq_main
	UPPER_RANDOM_BOUND			= UpperBoundRandomValue;	//used in lvq_main
	ULBOUNDPRECISION			= RandomValueStep;	//used in lvq_main
	NUMBEROFDATAPERSAMPLE		= NumberOfDataKeptPerSample;	//used in lvq
	//LOAD_WEIGHTS_FROM_PATH		= LoadWeightsStructureFromPath;	//not used
	//FIRST_LAYER_SIZE			= NumberOfNeuronsForFirstLayer;	//not used
	FILTERING_OPTION_CHOSEN		= FilteringOption;			//used in file access
	NUMBER_OF_SAMPLE_PER_FILE	= NumberOfSamplePerFile;	//used in file access
	SUCCESS_RATE_THRESHOLD		= SuccessRateThreshold;		//used in lvq_main

	TRAIN_DB_NAME				= TrainDataBaseBinFile;	//used in lvq_main
	VC_DB_NAME					= VCDataBaseBinFile;	//used in lvq_main
	TEST_DB_NAME				= TestDBBinFile;	//used in lvq_main
	NUMBEROFREPSPERCLASS		= NumberOfRepsPerClass;
	NUMBEROFCLASSES				= NumberOfClasses;
	ALPHA						= Alpha;
	INITWEIGHTSMODE				= InitWeightsMode;

	/*Add File Associations for easier rellocation*/

	if (FilteringOption == constants::FILTERING_OPTION::OPTIONA)
	{
		TRAIN_DB_NAME += "_A";
		VC_DB_NAME	  += "_A";				
		TEST_DB_NAME  += "_A";		
	}
	else if (FilteringOption == constants::FILTERING_OPTION::OPTIONB)
	{
		TRAIN_DB_NAME += "_B";
		VC_DB_NAME	  += "_B";				
		TEST_DB_NAME  += "_B";	
	}
	else if (FilteringOption == constants::FILTERING_OPTION::OPTIONC)
	{
		TRAIN_DB_NAME += "_C";
		VC_DB_NAME	  += "_C";				
		TEST_DB_NAME  += "_C";	
	}

	if (NUMBER_OF_SAMPLE_PER_FILE == constants::NUMBER_OF_SAMPLES_ACCEPTED::NO_40_SAMPLES)
	{
		TRAIN_DB_NAME += "_40";
		VC_DB_NAME	  += "_40";				
		TEST_DB_NAME  += "_40";	
	}
	else if (NUMBER_OF_SAMPLE_PER_FILE == constants::NUMBER_OF_SAMPLES_ACCEPTED::NO_50_SAMPLES)
	{
		TRAIN_DB_NAME += "_50";
		VC_DB_NAME	  += "_50";				
		TEST_DB_NAME  += "_50";	
	}
	else if (NUMBER_OF_SAMPLE_PER_FILE == constants::NUMBER_OF_SAMPLES_ACCEPTED::NO_60_SAMPLES)
	{
		TRAIN_DB_NAME += "_60";
		VC_DB_NAME	  += "_60";				
		TEST_DB_NAME  += "_60";	
	}


	OUTPUT_LOGFILE				= "Log_LVQ_NB_CLASS("+std::to_string(NUMBEROFCLASSES)
										 + ")_REP(" + std::to_string(NUMBEROFREPSPERCLASS)
										 + ")_Alpha(" + std::to_string(ALPHA)
										 + ")_LB(" + std::to_string(LOWER_RANDOM_BOUND) 
										 + ")_UB("   + std::to_string(UPPER_RANDOM_BOUND) + ")_"
										 + std::to_string(NUMBEROFDATAPERSAMPLE) + "_Samples_" + TRAIN_DB_NAME;
}

void SavedFileSettings::WriteCurrentWeightsToBin(std::string MyPath, Neuron_Network& NNetwork)
{

	fstream BinFile(MyPath, ios::out | ios::binary);
	
	std::vector<WeightBinFile> LayerVector;
	int HiddenLayerIndex = 0;
	int LayerNumber;

	if (BinFile.is_open())
	{
		LayerVector.resize(NNetwork.NetworkLayerNumber);

		//Get Size for each element in the vector
		for (LayerNumber = 0; LayerNumber < NNetwork.NetworkLayerNumber; LayerNumber++)
		{
			if (NNetwork.NetworkLayerNumber == 2)
			{
				if (LayerNumber == 0)
				{
					LayerVector[LayerNumber].iSizeOfLayer = NNetwork.inputLayer.size();
					LayerVector[LayerNumber].ySizeOfLayer = NNetwork.firstLayer.size();
				}
				else if (LayerNumber == 1)
				{
					LayerVector[LayerNumber].iSizeOfLayer = NNetwork.firstLayer.size();
					LayerVector[LayerNumber].ySizeOfLayer = NNetwork.outputLayer.size();
				}
			}
			else if (NNetwork.NetworkLayerNumber == 3)
			{
				if (LayerNumber == 0)
				{
					LayerVector[LayerNumber].iSizeOfLayer = NNetwork.inputLayer.size();
					LayerVector[LayerNumber].ySizeOfLayer = NNetwork.firstLayer.size();
				}
				else if (LayerNumber > 0 && LayerNumber < NNetwork.NetworkLayerNumber - 1)
				{
					LayerVector[LayerNumber].iSizeOfLayer = NNetwork.firstLayer.size();
					LayerVector[LayerNumber].ySizeOfLayer = NNetwork.hiddenLayers[0].size();

				}
				else if (LayerNumber == NNetwork.NetworkLayerNumber - 1)
				{
					LayerVector[LayerNumber].iSizeOfLayer = NNetwork.hiddenLayers[0].size();
					LayerVector[LayerNumber].ySizeOfLayer = NNetwork.outputLayer.size();
				}
			}
			else if (NNetwork.NetworkLayerNumber > 3)
			{
				if (LayerNumber == 0)
				{
					LayerVector[LayerNumber].iSizeOfLayer = NNetwork.inputLayer.size();
					LayerVector[LayerNumber].ySizeOfLayer = NNetwork.firstLayer.size();
				}
				else if (LayerNumber > 0 && LayerNumber < NNetwork.NetworkLayerNumber - 1)
				{
					if (HiddenLayerIndex == 0)
					{
						LayerVector[LayerNumber].iSizeOfLayer = NNetwork.firstLayer.size();
						LayerVector[LayerNumber].ySizeOfLayer = NNetwork.hiddenLayers[0].size();
					}
					else if (LayerNumber > 1 )
					{
						LayerVector[LayerNumber].iSizeOfLayer = NNetwork.hiddenLayers[HiddenLayerIndex - 1].size();
						LayerVector[LayerNumber].ySizeOfLayer = NNetwork.hiddenLayers[HiddenLayerIndex].size();
					}

					HiddenLayerIndex++;
				}
				else if (LayerNumber == NNetwork.NetworkLayerNumber - 1)
				{
					LayerVector[LayerNumber].iSizeOfLayer = NNetwork.hiddenLayers[HiddenLayerIndex - 1].size();
					LayerVector[LayerNumber].ySizeOfLayer = NNetwork.outputLayer.size();
				}
			}
		}

		//1- Properties for the properties table
		BinFile.write((char *)&NNetwork.NetworkLayerNumber, sizeof(int));


		for (LayerNumber = 0; LayerNumber < NNetwork.NetworkLayerNumber; LayerNumber++)
		{
			BinFile.write((char *)&LayerVector[LayerNumber].iSizeOfLayer, sizeof(int));
			BinFile.write((char *)&LayerVector[LayerNumber].ySizeOfLayer, sizeof(int));
		}

		//3-Save our Weights
		for (LayerNumber = 0; LayerNumber < NNetwork.NetworkLayerNumber; LayerNumber++)
			for (int i = 0; i < LayerVector[LayerNumber].iSizeOfLayer; i++)
				for (int j = 0; j < LayerVector[LayerNumber].ySizeOfLayer; j++)
					BinFile.write((char *)&NNetwork.currentweights[LayerNumber][i][j], sizeof(float));

		BinFile.close();
	}
	else
		cout << __func__ << ": Cannot Write to file '" << MyPath << endl;
}

void SavedFileSettings::ReadCurrentWeightsFromBin(std::string MyPath, Neuron_Network& NNetwork)
{
	fstream BinFile(MyPath, ios::in | ios::binary);
	int HiddenLayerIndex = 0;

	std::vector<WeightBinFile> LayerVector;

	if (BinFile.is_open())
	{
		BinFile.seekg(0);
		//Load Property Table sizes
		BinFile.read((char *)&NNetwork.NetworkLayerNumber, sizeof(int));

		LayerVector.resize(NNetwork.NetworkLayerNumber);

		NNetwork.SetNetworkLayers(NNetwork.NetworkLayerNumber);
		NNetwork.currentweights.resize(NNetwork.NetworkLayerNumber);
		NNetwork.deltaweights.resize(NNetwork.NetworkLayerNumber);

		for (int LayerNumber = 0; LayerNumber < NNetwork.NetworkLayerNumber; LayerNumber++)
		{
			BinFile.read((char *)&LayerVector[LayerNumber].iSizeOfLayer, sizeof(int));
			BinFile.read((char *)&LayerVector[LayerNumber].ySizeOfLayer, sizeof(int));
		}


		//3-Save our Weights
		for (int LayerNumber = 0; LayerNumber < NNetwork.NetworkLayerNumber; LayerNumber++)
		{
			for (int i = 0; i < LayerVector[LayerNumber].iSizeOfLayer; i++)
			{
				NNetwork.currentweights[LayerNumber].resize(LayerVector[LayerNumber].iSizeOfLayer);
				NNetwork.deltaweights[LayerNumber].resize(LayerVector[LayerNumber].iSizeOfLayer);

				for (int j = 0; j < LayerVector[LayerNumber].ySizeOfLayer; j++)
				{
					NNetwork.currentweights[LayerNumber][i].push_back(0);
					NNetwork.deltaweights[LayerNumber][i].push_back(0);
					BinFile.read((char *)&NNetwork.currentweights[LayerNumber][i][j], sizeof(float));
				}
			}
		}

		BinFile.close();

		//Create the rest of the neural Network according to the weights fetched from bin
		for (int LayerNumber = 0; LayerNumber < NNetwork.NetworkLayerNumber; LayerNumber++)
		{
			if (NNetwork.NetworkLayerNumber == 2)
			{
				if (LayerNumber == 0)
				{
					NNetwork.inputLayer.resize(LayerVector[LayerNumber].iSizeOfLayer);
					NNetwork.firstLayer.resize(LayerVector[LayerNumber].ySizeOfLayer);
				}
				else if (LayerNumber == 1)
				{
					NNetwork.firstLayer.resize(LayerVector[LayerNumber].iSizeOfLayer);
					NNetwork.outputLayer.resize(LayerVector[LayerNumber].ySizeOfLayer);
				}
			}
			else if (NNetwork.NetworkLayerNumber == 3)
			{
				if (LayerNumber == 0)
				{
					NNetwork.inputLayer.resize(LayerVector[LayerNumber].iSizeOfLayer);
					NNetwork.firstLayer.resize(LayerVector[LayerNumber].ySizeOfLayer);
				}
				else if (LayerNumber > 0 && LayerNumber < NNetwork.NetworkLayerNumber - 1)
				{
					NNetwork.firstLayer.resize(LayerVector[LayerNumber].iSizeOfLayer);
					NNetwork.hiddenLayers[0].resize(LayerVector[LayerNumber].ySizeOfLayer);

				}
				else if (LayerNumber == NNetwork.NetworkLayerNumber - 1)
				{
					NNetwork.hiddenLayers[0].resize(LayerVector[LayerNumber].iSizeOfLayer);
					NNetwork.outputLayer.resize(LayerVector[LayerNumber].ySizeOfLayer);
				}
			}
			else if (NNetwork.NetworkLayerNumber > 3)
			{
				if (LayerNumber == 0)
				{
					NNetwork.inputLayer.resize(LayerVector[LayerNumber].iSizeOfLayer);
					NNetwork.firstLayer.resize(LayerVector[LayerNumber].ySizeOfLayer);
				}
				else if (LayerNumber > 0 && LayerNumber < NNetwork.NetworkLayerNumber - 1)
				{
					if (HiddenLayerIndex == 0)
					{
						NNetwork.firstLayer.resize(LayerVector[LayerNumber].iSizeOfLayer);
						NNetwork.hiddenLayers[0].resize(LayerVector[LayerNumber].ySizeOfLayer);
					}
					else if (LayerNumber > 1 )
					{
						NNetwork.hiddenLayers[HiddenLayerIndex - 1].resize(LayerVector[LayerNumber].iSizeOfLayer);
						NNetwork.hiddenLayers[HiddenLayerIndex].resize(LayerVector[LayerNumber].ySizeOfLayer);
					}

					HiddenLayerIndex++;
				}
				else if (LayerNumber == NNetwork.NetworkLayerNumber - 1)
				{
					NNetwork.hiddenLayers[HiddenLayerIndex - 1].resize(LayerVector[LayerNumber].iSizeOfLayer);
					NNetwork.outputLayer.resize(LayerVector[LayerNumber].ySizeOfLayer);
				}
			}
		}
	}
	else
		cout << __func__ << ": Cannot Read to file '" << MyPath << endl;
}

void Neuron::UseActivationFunction(ACTIVATION_FUNCTION ChosenFunction, float i_node )
{
	// some activation function and their derivative on this site https://en.wikipedia.org/wiki/Activation_function

	if (ChosenFunction == SIGMOID )
	{
		output =  1 / (1 + exp(-(i_node)));
	}
	else if (ChosenFunction == ACTIVATION_FUNCTION::GAUSSIAN)
	{
		output = exp(-((i_node*i_node)));
	}
	else if (ChosenFunction == ACTIVATION_FUNCTION::TANH)
	{
		output = (2 / (1+exp(-2*i_node))) - 1;
	}
	else if (ChosenFunction == ACTIVATION_FUNCTION::BENTIDENTITY)
	{
		output = (sqrt(i_node*i_node+1)-1)/2+i_node;
	}
	else
		output = 0;
	
}

float Neuron::UseDerivativeActivationFunction(ACTIVATION_FUNCTION ChosenFunction, float i_node)
{
	// some activation function and their derivative on this site https://en.wikipedia.org/wiki/Activation_function

	if (ChosenFunction == SIGMOID)
	{
		return (1 / (1 + exp(-(i_node))))*(1 - (1 / (1 + exp(-(i_node))))); //f(x)(1-f(x))
	}
	else if (ChosenFunction == GAUSSIAN)
	{
		return -2 * i_node*exp(-(i_node*i_node));//-2x(e^(-x^2))
	}
	else if (ChosenFunction == TANH)
	{
		return 1 - ((2 / (1 + exp(-2 * i_node))) - 1)*((2 / (1 + exp(-2 * i_node))) - 1);//1-f(x)^2
	}
	else if (ChosenFunction == BENTIDENTITY)
	{
		return i_node/(2*sqrt(i_node*i_node+1))+1;//x/(2*sqrt(x^2+1))+1
	}
	else
		return 0;
}

int Neuron_Network::SetNetworkLayers(int NumberLayers)
{
	if (NumberLayers >= 2)
	{
		hiddenLayers.resize(NumberLayers - 2);				//Resize number of hidden layers
		NetworkLayerNumber = NumberLayers;					//Set our network size
		return hiddenLayers.size();
	}
	else
	{
		cout << __func__ << ": Has to be equal to or more than 2 layers.. Aborting" << endl;
		cout << __func__ << ": Press enter to continue...";
		cin.ignore();
		exit(EXIT_FAILURE);
	}

}

int Neuron_Network::SetLayerSize(int LayerNumber, int NumberNeurons)
{
	if (NetworkLayerNumber == 2)							//If we only have two layers we omit hidden layers
	{
		if (LayerNumber == 0)
		{
			firstLayer.resize(NumberNeurons);
			return firstLayer.size();
		}
		else if (LayerNumber == 1)
		{
			outputLayer.resize(NumberNeurons);
			return outputLayer.size();
		}
		else
			return 0;
	}
	else																//Hidden layer mode
	{
		if (LayerNumber == 0)											//first layer chosen
		{
			firstLayer.resize(NumberNeurons);
			return firstLayer.size();
		}
		else if (LayerNumber == NetworkLayerNumber - 1)					//Output Layer Chosen
		{
			outputLayer.resize(NumberNeurons);
			return outputLayer.size();
		}
		else if (LayerNumber > 0 && LayerNumber < NetworkLayerNumber)	//Hidden Layers chosen
		{
			hiddenLayers[LayerNumber - 1].resize(NumberNeurons);
			return hiddenLayers[LayerNumber - 1].size();
		}
		else
		{
			cout << __func__ << ": Layer (" << LayerNumber << ") Not found" << endl;
			return 0;
		}
	}
}

int Neuron_Network::SetInputSize(int InputNumber)
{
	inputLayer.resize(InputNumber);
	return inputLayer.size();
}

int Neuron_Network::SetInputTestValues()
{
	for (int i = 0; i < inputLayer.size(); i++)
		inputLayer[i].output = (float)i;
	return 1; 
}

int Neuron_Network::CreateNetworkWeights()
{
	int current_layer_number;
	currentweights.resize(NetworkLayerNumber);																					//Create Weights per layer
	deltaweights.resize(NetworkLayerNumber);
	if (NetworkLayerNumber == 2)
	{
		for (current_layer_number = 0; current_layer_number < currentweights.size(); current_layer_number++)
		{
			if (current_layer_number == 0)
			{
				currentweights[0].resize(inputLayer.size());																	//Resize i value for unputweight
				deltaweights[0].resize(inputLayer.size());

				for (int j = 0; j < inputLayer.size(); j++)
				{
					currentweights[0][j].resize(firstLayer.size());
					deltaweights[0][j].resize(firstLayer.size());
				}
			}
			else
			{
				currentweights[1].resize(firstLayer.size());																	//Resize i values for input weights
				deltaweights[1].resize(firstLayer.size());

				for (int j = 0; j < firstLayer.size(); j++)																		//Resize j values for input weights
				{
					currentweights[1][j].resize(outputLayer.size());
					deltaweights[1][j].resize(outputLayer.size());
				}
			}
		}
	}
	else if (NetworkLayerNumber == 3)																							//if we have 3 layers
	{
		for (current_layer_number = 0; current_layer_number < currentweights.size(); current_layer_number++)				
		{
			if (current_layer_number == 0)
			{
				currentweights[0].resize(inputLayer.size());																	//Resize i value for unputweight
				deltaweights[0].resize(inputLayer.size());

				for (int j = 0; j < inputLayer.size(); j++)
				{
					currentweights[0][j].resize(firstLayer.size());
					deltaweights[0][j].resize(firstLayer.size());
				}

			}
			else if (current_layer_number == 1)
			{
				currentweights[current_layer_number].resize(firstLayer.size());													//Resize i value for the 1 hidden layer
				deltaweights[current_layer_number].resize(firstLayer.size());

				for (int j = 0; j < firstLayer.size(); j++)
				{
					currentweights[1][j].resize(hiddenLayers[0].size());														//Resize j value for the 1 hidden layer
					deltaweights[1][j].resize(hiddenLayers[0].size());
				}

			}
			else if (current_layer_number == 2)
			{
				currentweights[current_layer_number].resize(hiddenLayers[0].size());											//Resize i value for output layer
				deltaweights[current_layer_number].resize(hiddenLayers[0].size());

				for (int j = 0; j < hiddenLayers[0].size(); j++)
				{
					currentweights[current_layer_number][j].resize(outputLayer.size());											//Resize j value for output layer		
					deltaweights[current_layer_number][j].resize(outputLayer.size());
				}
			}
		}
	}
	else if (NetworkLayerNumber > 3)																							//Anything else bigger than 3 layers is the same
	{
		for (current_layer_number = 0; current_layer_number < currentweights.size(); current_layer_number++)					//Create all i values for first layer
		{
			if (current_layer_number == 0)
			{
				currentweights[0].resize(inputLayer.size());																	//Resize i value for unputweight
				deltaweights[0].resize(inputLayer.size());

				for (int j = 0; j < inputLayer.size(); j++)
				{
					currentweights[0][j].resize(firstLayer.size());
					deltaweights[0][j].resize(firstLayer.size());
				}
			}
			else if (current_layer_number > 0 && current_layer_number < NetworkLayerNumber - 1)									//Hidden layers
			{
				if (current_layer_number == 1)
				{
					currentweights[1].resize(firstLayer.size());																//Resize i value for the 1 hidden layer
					deltaweights[1].resize(firstLayer.size());

					for (int j = 0; j < firstLayer.size(); j++)
					{
						currentweights[1][j].resize(hiddenLayers[0].size());
						deltaweights[1][j].resize(hiddenLayers[0].size());
					}
						
				}
				else if (current_layer_number > 1)
				{
					currentweights[current_layer_number].resize(hiddenLayers[current_layer_number - 2].size());					//Resize i value for the 1 hidden layer
					deltaweights[current_layer_number].resize(hiddenLayers[current_layer_number - 2].size());

					for (int j = 0; j < hiddenLayers[current_layer_number - 2].size(); j++)
					{
						currentweights[current_layer_number][j].resize(hiddenLayers[current_layer_number - 1].size());
						deltaweights[current_layer_number][j].resize(hiddenLayers[current_layer_number - 1].size());
					}

				}

			}
			else
			{
				currentweights[current_layer_number].resize(hiddenLayers[hiddenLayers.size() - 1].size());						//Resize i value for output layer
				deltaweights[current_layer_number].resize(hiddenLayers[hiddenLayers.size() - 1].size());

				for (int j = 0; j < hiddenLayers[hiddenLayers.size() - 1].size(); j++)
				{
					currentweights[current_layer_number][j].resize(outputLayer.size());											//Resize j value for output layer	
					deltaweights[current_layer_number][j].resize(outputLayer.size());
				}

			}
		}
	}

	return currentweights.size();
}

int Neuron_Network::InitWeights(INIT_WEIGHTS_FROM Mode)
{

	cout << __func__ << ": Initializing Weights using " << " LB(" << LOWER_RANDOM_BOUND << ") UB("   << UPPER_RANDOM_BOUND << ") " << endl;

	int current_layer_number, i, j;
	if (Mode == INIT_WEIGHTS_FROM::DEFAULT)			//According to DEFAULT_WEIGHT_VALUES constant
	{
		for (current_layer_number = 0; current_layer_number < currentweights.size(); current_layer_number++)			//Create all j values of weights
			for (i = 0; i < currentweights[current_layer_number].size(); i++)
				for (j = 0; j < currentweights[current_layer_number][i].size(); j++)
					currentweights[current_layer_number][i][j] = (float)DEFAULT_WEIGHT_VALUES;
	}
	else if (Mode == INIT_WEIGHTS_FROM::RANDOM)		//Random Values between LOWER_RANDOM_BOUND & UPPER_RANDOM_BOUND constants
	{
		for (current_layer_number = 0; current_layer_number < currentweights.size(); current_layer_number++)			//Create all j values of weights
			for (i = 0; i < currentweights[current_layer_number].size(); i++)
				for (j = 0; j < currentweights[current_layer_number][i].size(); j++)
				{
					currentweights[current_layer_number][i][j] = ((float)(rand() % TOTALRANDOMQTY + (ADJUSTEDLOWERBOUND))) / (float)ULBOUNDPRECISION;
					//cout << __func__ << ": Randomizing weight on Layer (" << current_layer_number << ")  i(" << i  << ")  j(" << j << ")" << " with " << currentweights[current_layer_number][i][j] << endl;
				}
	}		
	else if (Mode == INIT_WEIGHTS_FROM::FILE_MODE)		//According to file
	{
		//If we have time...
	}

	return currentweights[0][0].size();										//Upon success, the function returns the quantity of input weights
}

void Neuron_Network::ClearAllTempData()
{
	int current_layer_number, current_neuron;
	int hiddenlayerindex = 0;

	for (current_layer_number = 0; current_layer_number <= NetworkLayerNumber; current_layer_number++)	//Go through all layers
		if (current_layer_number == 0)
		{
			for (current_neuron = 0; current_neuron < firstLayer.size(); current_neuron++)
			{
				firstLayer[current_neuron].i_node = 0;
				firstLayer[current_neuron].output = 0;
				firstLayer[current_neuron].error_sum = 0;
			}
		}
		else if (current_layer_number > 0 && current_layer_number < NetworkLayerNumber-1)							
		{
			for (current_neuron = 0; current_neuron < hiddenLayers[hiddenlayerindex].size(); current_neuron++)
			{
				hiddenLayers[hiddenlayerindex][current_neuron].i_node = 0;
				hiddenLayers[hiddenlayerindex][current_neuron].output = 0;
				hiddenLayers[hiddenlayerindex][current_neuron].error_sum = 0;
			}
			hiddenlayerindex++;																			//move up 1 hidden layer
		}
		else if (current_layer_number == NetworkLayerNumber-1)
		{
			for (current_neuron = 0; current_neuron < outputLayer.size(); current_neuron++)
			{
				outputLayer[current_neuron].i_node = 0;
				outputLayer[current_neuron].output = 0;
				outputLayer[current_neuron].error_sum = 0;
			}
		}
}

int Neuron_Network::LoadInputsFromDataBase(std::vector<File_Class>& db_structure, int Current_File_number)
{
	inputLayer.resize(db_structure[Current_File_number].float_parsed_rawdata.size()*NUMBEROFDATAPERSAMPLE);
	int InputLayerIndex = 0;

	for (int Sample = 0; Sample < db_structure[Current_File_number].float_parsed_rawdata.size(); Sample++)
		for (int DataIndex = 0; DataIndex < NUMBEROFDATAPERSAMPLE; DataIndex++)
		{
			inputLayer[InputLayerIndex].output = db_structure[Current_File_number].float_parsed_rawdata[Sample][DataIndex];
			InputLayerIndex++;
		}

	return inputLayer.size();
}

int Neuron_Network::UpdateAllNetworkActivationFunctions(ACTIVATION_FUNCTION ActivationFunction)	//phase1
{
	int HiddenLayerIndex = 0;

	if (NetworkLayerNumber == 2)
	{
		for (int current_layer_number = 0; current_layer_number < NetworkLayerNumber; current_layer_number++)
		{
			if (current_layer_number == 0)
			{
				for (int current_neuron = 0; current_neuron < firstLayer.size(); current_neuron++)							//Looping through all neurons in layer
				{
					for (int i = 0; i < inputLayer.size(); i++)																//Looping through all i weights
						firstLayer[current_neuron].i_node += (inputLayer[i].output*currentweights[current_layer_number][i][current_neuron]);

					firstLayer[current_neuron].UseActivationFunction(ActivationFunction, firstLayer[current_neuron].i_node);
				}
			}
			else if (current_layer_number == 1)
			{
				for (int current_neuron = 0; current_neuron < outputLayer.size(); current_neuron++)							
				{
					for (int i = 0; i < firstLayer.size(); i++)																
						outputLayer[current_neuron].i_node += (firstLayer[i].output*currentweights[current_layer_number][i][current_neuron]);

					outputLayer[current_neuron].UseActivationFunction(ActivationFunction, outputLayer[current_neuron].i_node);
				}
			}
		}
	}
	else if (NetworkLayerNumber > 2)																							//Anything else bigger than 2 layers is the same
	{
		for (int current_layer_number = 0; current_layer_number < NetworkLayerNumber; current_layer_number++)					//Create all i values for first layer
		{
			if (current_layer_number == 0)
			{
				for (int current_neuron = 0; current_neuron < firstLayer.size(); current_neuron++)
				{
					for (int i = 0; i < inputLayer.size(); i++)
						firstLayer[current_neuron].i_node += (inputLayer[i].output*currentweights[current_layer_number][i][current_neuron]);

					firstLayer[current_neuron].UseActivationFunction(ActivationFunction, firstLayer[current_neuron].i_node);
				}
			}
			else if (current_layer_number > 0 && current_layer_number != NetworkLayerNumber - 1)								//Hidden layers
			{
				if (HiddenLayerIndex == 0)
				{
					for (int current_neuron = 0; current_neuron < hiddenLayers[HiddenLayerIndex].size(); current_neuron++)
					{
						for (int i = 0; i < firstLayer.size(); i++)
							hiddenLayers[HiddenLayerIndex][current_neuron].i_node += (firstLayer[i].output*currentweights[current_layer_number][i][current_neuron]);

						hiddenLayers[HiddenLayerIndex][current_neuron].UseActivationFunction(ActivationFunction, hiddenLayers[HiddenLayerIndex][current_neuron].i_node);
					}
				}
				else if (HiddenLayerIndex > 0)
				{
					for (int current_neuron = 0; current_neuron < hiddenLayers[HiddenLayerIndex].size(); current_neuron++)
					{
						for (int i = 0; i < hiddenLayers[HiddenLayerIndex - 1].size(); i++)
							hiddenLayers[HiddenLayerIndex][current_neuron].i_node += (hiddenLayers[HiddenLayerIndex - 1][i].output*currentweights[current_layer_number][i][current_neuron]);

						hiddenLayers[HiddenLayerIndex][current_neuron].UseActivationFunction(ActivationFunction, hiddenLayers[HiddenLayerIndex][current_neuron].i_node);
					}
				}

				HiddenLayerIndex++;
			}
			else if(current_layer_number == NetworkLayerNumber - 1)
			{
				for (int current_neuron = 0; current_neuron < outputLayer.size(); current_neuron++)
				{
					for (int i = 0; i < hiddenLayers[HiddenLayerIndex - 1].size(); i++)
						outputLayer[current_neuron].i_node += (hiddenLayers[HiddenLayerIndex - 1][i].output *currentweights[current_layer_number][i][current_neuron]);

					outputLayer[current_neuron].UseActivationFunction(ActivationFunction, outputLayer[current_neuron].i_node);
				}
			}
		}
	}

	return 1;
}																					   

int Neuron_Network::SetWantedOutput(std::vector<File_Class>& db_structure, int Current_File_number, int Number_of_Output)
{
	WantedOutput.resize(Number_of_Output);
	switch (db_structure[Current_File_number].ExpectedOutput)
	{
	case 0:
		for (int i = 0; i < Number_of_Output; i++)
		{
			if (i == 0)
			{
				WantedOutput[i] = 1;
			}
			else
			{
				WantedOutput[i] = 0;
			}
		}
		break;

	case 1:
		for (int i = 0; i < Number_of_Output; i++)
		{
			if (i == 1)
			{
				WantedOutput[i] = 1;
			}
			else
			{
				WantedOutput[i] = 0;
			}
		}
		break;

	case 2:
		for (int i = 0; i < Number_of_Output; i++)
		{
			if (i == 2)
			{
				WantedOutput[i] = 1;
			}
			else
			{
				WantedOutput[i] = 0;
			}
		}
		break;

	case 3:
		for (int i = 0; i < Number_of_Output; i++)
		{
			if (i == 3)
			{
				WantedOutput[i] = 1;
			}
			else
			{
				WantedOutput[i] = 0;
			}
		}
		break;

	case 4:
		for (int i = 0; i < Number_of_Output; i++)
		{
			if (i == 4)
			{
				WantedOutput[i] = 1;
			}
			else
			{
				WantedOutput[i] = 0;
			}
		}
		break;

	case 5:
		for (int i = 0; i < Number_of_Output; i++)
		{
			if (i == 5)
			{
				WantedOutput[i] = 1;
			}
			else
			{
				WantedOutput[i] = 0;
			}
		}
		break;
	case 6:
		for (int i = 0; i < Number_of_Output; i++)
		{
			if (i == 6)
			{
				WantedOutput[i] = 1;
			}
			else
			{
				WantedOutput[i] = 0;
			}
		}
		break;
	case 7:
		for (int i = 0; i < Number_of_Output; i++)
		{
			if (i == 7)
			{
				WantedOutput[i] = 1;
			}
			else
			{
				WantedOutput[i] = 0;
			}
		}
		break;
	case 8:
		for (int i = 0; i < Number_of_Output; i++)
		{
			if (i == 8)
			{
				WantedOutput[i] = 1;
			}
			else
			{
				WantedOutput[i] = 0;
			}
		}
		break;
	case 9:
		for (int i = 0; i < Number_of_Output; i++)
		{
			if (i == 9)
			{
				WantedOutput[i] = 1;
			}
			else
			{
				WantedOutput[i] = 0;
			}
		}
		break;
	default:
		cout << "not a number from 0 to 9\n";
		break;
	}

	return 1;

}

float Neuron_Network::CheckForErrors(std::vector<File_Class>& db_structure, int Current_File_number)
{
	int i, k = 0;
	float highestoutput=0;

	for (int j = 0; j < outputLayer.size(); j++)
	{
		if (outputLayer[j].output > highestoutput)
		{
			highestoutput = outputLayer[j].output;
			k = j;
		}

	}

	
	if (db_structure[Current_File_number].ExpectedOutput == k)
	{
		NumberOfTrainingSucceededOutput++;
		NumberOfCrossValidationSucceededOutput++;
		NumberOfTestSucceededOutput++;
		return 0;
	}
	else
		return 1;																	//returns the difference

	return 1;
}

int Neuron_Network::UpdateAllSignalErrors(ACTIVATION_FUNCTION ActivationFunction)
{
	int current_layer_number;
	int hiddenlayerindex = hiddenLayers.size()-1;
	if (NetworkLayerNumber == 2)
	{
		for (current_layer_number = NetworkLayerNumber - 1; current_layer_number >= 0; current_layer_number--)
		{
			if (current_layer_number == NetworkLayerNumber - 1)																	
			{
				for (int current_neuron = 0; current_neuron < outputLayer.size(); current_neuron++)
				{

					outputLayer[current_neuron].error = (WantedOutput[current_neuron] - outputLayer[current_neuron].output)*outputLayer[current_neuron].UseDerivativeActivationFunction(ActivationFunction, outputLayer[current_neuron].i_node);

				}
			}
			else if (current_layer_number == 0)																
			{
				for (int current_neuron = 0; current_neuron < firstLayer.size(); current_neuron++)			//Go through every neuron per layer
				{
					for (int i = 0; i < outputLayer.size(); i++)
						firstLayer[current_neuron].error_sum += (outputLayer[i].error*currentweights[current_layer_number + 1][current_neuron][i]);
					firstLayer[current_neuron].error = (firstLayer[current_neuron].UseDerivativeActivationFunction(ActivationFunction, firstLayer[current_neuron].i_node)*firstLayer[current_neuron].error_sum);
				}
			}
		}
	}
	else if (NetworkLayerNumber > 2)
	{
		for (current_layer_number = NetworkLayerNumber - 1; current_layer_number >= 0; current_layer_number--)	//Go through all layers but the last layer first
		{
			if (current_layer_number == NetworkLayerNumber - 1)																	
			{
				for (int current_neuron = 0; current_neuron < outputLayer.size(); current_neuron++)
				{

					outputLayer[current_neuron].error = (WantedOutput[current_neuron] - outputLayer[current_neuron].output)*outputLayer[current_neuron].UseDerivativeActivationFunction(ActivationFunction, outputLayer[current_neuron].i_node);

				}
			}
			else if (current_layer_number < NetworkLayerNumber - 1 && current_layer_number > 0)								//Tend to all hidden layers other than output layer and first layer
			{

				for (int current_neuron = 0; current_neuron < hiddenLayers[hiddenlayerindex].size(); current_neuron++)		//Go through every neuron per layer
				{
					if (hiddenlayerindex == hiddenLayers.size() - 1)
					{
						for (int i = 0; i < outputLayer.size(); i++)
							hiddenLayers[hiddenlayerindex][current_neuron].error_sum += (outputLayer[i].error*currentweights[current_layer_number + 1][current_neuron][i]);
					}
					else if (hiddenlayerindex < hiddenLayers.size() - 1)
					{
						for (int i = 0; i < hiddenLayers[hiddenlayerindex+1].size(); i++)
							hiddenLayers[hiddenlayerindex][current_neuron].error_sum += (hiddenLayers[hiddenlayerindex + 1][i].error*currentweights[current_layer_number + 1][current_neuron][i]);
					}

					hiddenLayers[hiddenlayerindex][current_neuron].error = hiddenLayers[hiddenlayerindex][current_neuron].UseDerivativeActivationFunction(ActivationFunction, hiddenLayers[hiddenlayerindex][current_neuron].i_node)*hiddenLayers[hiddenlayerindex][current_neuron].error_sum;
				}

				hiddenlayerindex--;																			//move down 1 hidden layer
			}
			else if (current_layer_number == 0)																
			{
				for (int current_neuron = 0; current_neuron < firstLayer.size(); current_neuron++)			//Go through every neuron per layer
				{
					for (int i = 0; i < hiddenLayers[0].size(); i++)
						firstLayer[current_neuron].error_sum += (hiddenLayers[0][i].error*currentweights[current_layer_number + 1][current_neuron][i]);
					firstLayer[current_neuron].error = (firstLayer[current_neuron].UseDerivativeActivationFunction(ActivationFunction, firstLayer[current_neuron].i_node)*firstLayer[current_neuron].error_sum);
				}
			}
		}
	}
	return 1;
}	

int Neuron_Network::CalculateNewWeights(float learningValue)
{
	//finding the correct weight variation
   /*Phase3*/
   // for first layer : delta_weight = signal error * n * x
   // for other layers : delta_weight = signal error * n * a
	int current_layer_number, current_neuron, i;
	int hiddenlayerindex = 0;
	if (NetworkLayerNumber==2)
	{
		for (current_layer_number = 0; current_layer_number <= NetworkLayerNumber - 1; current_layer_number++)	//Go through all layers
		{
			if (current_layer_number == 0)																	//Tend to First layer
			{
				for (current_neuron = 0; current_neuron < firstLayer.size(); current_neuron++)
				{
					for (i = 0; i < inputLayer.size(); i++)
						deltaweights[current_layer_number][i][current_neuron] = (inputLayer[i].output*learningValue*firstLayer[current_neuron].error);

				}
			}
			else if (current_layer_number == NetworkLayerNumber - 1)
			{
				for (current_neuron = 0; current_neuron < outputLayer.size(); current_neuron++)		//Go through every neuron per layer
				{
					for (i = 0; i < firstLayer.size(); i++)
						deltaweights[current_layer_number][i][current_neuron] = (firstLayer[i].output*learningValue*outputLayer[current_neuron].error);
				}
			}

		}
	}
	else if(NetworkLayerNumber>2)
	{
		for (current_layer_number = 0; current_layer_number <= NetworkLayerNumber-1; current_layer_number++)	//Go through all layers
		{
			if (current_layer_number == 0)																	//Tend to First layer
			{
				for (current_neuron = 0; current_neuron < firstLayer.size(); current_neuron++)
				{
					for (i = 0; i < inputLayer.size(); i++)
						deltaweights[current_layer_number][i][current_neuron] = (inputLayer[i].output*learningValue*firstLayer[current_neuron].error);

				}
			}
			else if (current_layer_number > 0 && current_layer_number < NetworkLayerNumber-1)								//Tend to all hidden layers
			{
				for (current_neuron = 0; current_neuron < hiddenLayers[hiddenlayerindex].size(); current_neuron++)		//Go through every neuron per layer
				{
					if (hiddenlayerindex == 0)
					{
						for (i = 0; i < firstLayer.size(); i++)
							deltaweights[current_layer_number][i][current_neuron] = (firstLayer[i].output*learningValue*hiddenLayers[hiddenlayerindex][current_neuron].error);
					}
					else
					{
						for (i = 0; i < hiddenLayers[hiddenlayerindex].size(); i++)
							deltaweights[current_layer_number][i][current_neuron] = (hiddenLayers[hiddenlayerindex - 1][i].output*learningValue*hiddenLayers[hiddenlayerindex][current_neuron].error);
					}
				}

				hiddenlayerindex++;																			//move up 1 hidden layer
			}
			else if (current_layer_number == NetworkLayerNumber - 1)
			{
				for (current_neuron = 0; current_neuron < outputLayer.size(); current_neuron++)		//Go through every neuron per layer
				{
					for (i = 0; i < hiddenLayers[hiddenlayerindex-1].size(); i++)
						deltaweights[current_layer_number][i][current_neuron] = (hiddenLayers[hiddenlayerindex-1][i].output*learningValue*outputLayer[current_neuron].error);
				}
			}
		}
	}
	return 1;
}

int Neuron_Network::UpdateAllNewWeights()
{
	 // update weights
	 /*Phase4*/
	 // for all layers new weight=old weight + delta_weight
	 // change weights		
	int current_layer_number, current_neuron, i;
	int hiddenlayerindex = 0;
	if (NetworkLayerNumber == 2)
	{
		for (current_layer_number = 0; current_layer_number < NetworkLayerNumber; current_layer_number++)	//Go through all layers
		{

			if (current_layer_number == 0)																	//Tend to First layer
			{
				for (current_neuron = 0; current_neuron < firstLayer.size(); current_neuron++)
				{
					for (i = 0; i < inputLayer.size(); i++)
						currentweights[current_layer_number][i][current_neuron] += deltaweights[current_layer_number][i][current_neuron];

				}
			}
			else if (current_layer_number == NetworkLayerNumber - 1)
			{
				for (current_neuron = 0; current_neuron < outputLayer.size(); current_neuron++)
				{
					for (i = 0; i < outputLayer.size(); i++)
						currentweights[current_layer_number][i][current_neuron] += deltaweights[current_layer_number][i][current_neuron];
				}
			}
		}
	}
	else
	{
		for (current_layer_number = 0; current_layer_number < NetworkLayerNumber; current_layer_number++)	//Go through all layers
		{

			if (current_layer_number == 0)																	//Tend to First layer
			{
				for (current_neuron = 0; current_neuron < firstLayer.size(); current_neuron++)
				{
					for (i = 0; i < inputLayer.size(); i++)
						currentweights[current_layer_number][i][current_neuron] += deltaweights[current_layer_number][i][current_neuron];

				}
			}
			else if (current_layer_number > 0 && current_layer_number < NetworkLayerNumber - 1)
			{
				for (current_neuron = 0; current_neuron < hiddenLayers[hiddenlayerindex].size(); current_neuron++)
				{
					if (hiddenlayerindex == 0)
					{
						for (i = 0; i < firstLayer.size(); i++)
							currentweights[current_layer_number][i][current_neuron] += deltaweights[current_layer_number][i][current_neuron];
					}
					else
					{
						for (i = 0; i < hiddenLayers[hiddenlayerindex - 1].size(); i++)
							currentweights[current_layer_number][i][current_neuron] += deltaweights[current_layer_number][i][current_neuron];
					}

				}

				hiddenlayerindex++;

			}
			else if (current_layer_number == NetworkLayerNumber - 1)
			{
				for (current_neuron = 0; current_neuron < outputLayer.size(); current_neuron++)
				{
					for (i = 0; i < hiddenLayers[hiddenlayerindex - 1].size(); i++)
						currentweights[current_layer_number][i][current_neuron] += deltaweights[current_layer_number][i][current_neuron];
				}
			}
		}
	}
	return 1;
}

int Neuron_Network::TrainNetwork(std::vector<File_Class>& db_structure, SavedFileSettings MyConfig)
{
	int randomfile = 0;
	for (int i = 0; i < db_structure.size(); i++)												//Number of times to train the network
	{
		randomfile = rand() % (db_structure.size()-1);											//Pre Phase
		ClearAllTempData();
		LoadInputsFromDataBase(db_structure, randomfile);
		

		UpdateAllNetworkActivationFunctions(MyConfig.ChosenActivationFunction);					//Phase1


		SetWantedOutput(db_structure, randomfile, NUMBER_OF_OUTPUT);

		if (CheckForErrors(db_structure, randomfile))											//Check for errors
		{
			//Correction Phases
			UpdateAllSignalErrors(MyConfig.ChosenActivationFunction);							//Phase2
			CalculateNewWeights(MyConfig.LearningValue_N);										//Phase3
			UpdateAllNewWeights();																//Phase4
		}
	}
	return 1;
}

int Neuron_Network::TestNetwork(std::vector<File_Class>& db_structure, SavedFileSettings MyConfig)
{
	for (int i = 0; i < db_structure.size(); i++)												//Number of times to train the network
	{
																							//Pre Phase
		ClearAllTempData();
		LoadInputsFromDataBase(db_structure, i);

		UpdateAllNetworkActivationFunctions(MyConfig.ChosenActivationFunction);							//Phase1
		SetWantedOutput(db_structure, i, NUMBER_OF_OUTPUT);
		CheckForErrors(db_structure, i);															//Check for errors
		//WriteOutputValidation(OUTPUT_LOGFILE, db_structure, i, 0);

	}
	return 1;
}

void Neuron_Network::WriteOutputValidation(std::string MyPath, std::vector<File_Class>& db_structure, int IterationNumber, int iterationNumbera)
{
	/*ofstream TargetFile;
	if(IterationNumber == 0)
		TargetFile.open(MyPath);
	else
		TargetFile.open(MyPath, std::ios::app);



	TargetFile << "____________________Iteration " << iterationNumbera << IterationNumber << "_____________________" << endl;

	/*TargetFile << "Expected Value: " << db_structure[IterationNumber].ExpectedOutput << endl;

	for (int i = 0; i < outputLayer.size(); i++)
		TargetFile << "Iteration Count = " << IterationNumber << " - Output[" << i << "] = " << outputLayer[i].output << endl;

	for (int i = 0; i < WantedOutput.size(); i++)
		TargetFile << "With WantedOutput[" << i << "] = " << WantedOutput[i] << endl;*/

	/*TargetFile << "Efficiency = " << (float)(100 * (float)NumberOfTrainingSucceededOutput / (float)db_structure.size()) << "%" << endl;
	TargetFile << "____________________________________________________________________________" << endl << endl;

	TargetFile.close();*/
}


