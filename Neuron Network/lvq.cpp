#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <cmath>
#include "NeuralNet.h"
#include "file_access.h"
#include "lvq.h"


using namespace std;

void TYPE_LVQ::WriteCurrentLVQWeightsToBin(std::string MyPath, int NumberOfRepresentationPerClass, int NumberOfClassifications, TYPE_LVQ& NNetwork)
{

	fstream BinFile(MyPath, ios::out | ios::binary);

	std::vector<WeightBinFileLVQ> Structure;
	int i;

	if (BinFile.is_open())
	{
		Structure.resize(NumberOfRepresentationPerClass);
		for (i = 0; i < NumberOfRepresentationPerClass; i++)
		{
			Structure[i].numberofrepperclass = NumberOfRepresentationPerClass;
			Structure[i].numberofclasses = NumberOfClassifications;
			Structure[i].sizeofinputvector = InputVector.size();
		}
		//1- Properties for the properties table



		for (i = 0; i < NumberOfRepresentationPerClass; i++)
		{
			BinFile.write((char *)&Structure[i].numberofrepperclass, sizeof(int));
			BinFile.write((char *)&Structure[i].numberofclasses, sizeof(int));
			BinFile.write((char *)&Structure[i].sizeofinputvector, sizeof(int));
		}

		//3-Save our Weights
		for (i = 0; i < NumberOfRepresentationPerClass; i++)
			for (int j = 0; j < NumberOfClassifications; j++)
				for(int k = 0; k < InputVector.size(); k++)
					BinFile.write((char *)&NNetwork.VectorWeights[i][j][k], sizeof(float));

		BinFile.close();
	}
	else
		cout << __func__ << ": Cannot Write to file '" << MyPath << endl;
}

void TYPE_LVQ::ReadCurrentLVQWeightsFromBin(std::string MyPath,int NumberOfRepresentationPerClass, int NumberOfClassifications, TYPE_LVQ& NNetwork)
{
	fstream BinFile(MyPath, ios::in | ios::binary);

	std::vector<WeightBinFileLVQ> Structure;

	if (BinFile.is_open())
	{
		BinFile.seekg(0);
		//Load Property Table sizes

		Structure.resize(NumberOfRepresentationPerClass);

		for (int i = 0; i < NumberOfRepresentationPerClass; i++)
		{
			BinFile.read((char *)&Structure[i].numberofrepperclass, sizeof(int));
			BinFile.read((char *)&Structure[i].numberofclasses, sizeof(int));
			BinFile.read((char *)&Structure[i].sizeofinputvector, sizeof(int));
		}
		VectorWeights.resize(Structure[0].numberofrepperclass);
		//3-Save our Weights
		for (int i = 0; i < Structure.size(); i++)
		{
			VectorWeights[i].resize(Structure[i].numberofclasses);
			for (int j = 0; j < Structure[i].numberofclasses; j++)
			{
				VectorWeights[i][j].resize(Structure[i].sizeofinputvector);
				for (int k = 0; k < Structure[i].sizeofinputvector; k++)
				{
					BinFile.read((char *)&NNetwork.VectorWeights[i][j][k], sizeof(float));
				}
			}
		}

		BinFile.close();
		
	}
	else
		cout << __func__ << ": Cannot Read to file '" << MyPath << endl;
}


void TYPE_LVQ::LoadInputsToVector(std::vector<File_Class>& db_structure, int Current_File_number) 
{


	InputVector.resize(db_structure[Current_File_number].float_parsed_rawdata.size()*NUMBEROFDATAPERSAMPLE);
	int InputVectorIndex = 0;

	for (int Sample = 0; Sample < db_structure[Current_File_number].float_parsed_rawdata.size(); Sample++)
		for (int DataIndex = 0; DataIndex < NUMBEROFDATAPERSAMPLE; DataIndex++)
		{
			InputVector[InputVectorIndex] = db_structure[Current_File_number].float_parsed_rawdata[Sample][DataIndex];
			InputVectorIndex++;
		}

	CurrentWantedOutput = db_structure[Current_File_number].ExpectedOutput;
}

void TYPE_LVQ::InitializeVariables(float AlphaValue)
{
	ShortestDistanceID.NumberOfrepresentation = 0;
	ShortestDistanceID.ClassNumber = 0;
	ShortestDistanceID.ShortestDistance = 0;
	Alpha = AlphaValue;
	CurrentDistance = 0;
}

void TYPE_LVQ::InitWeights(std::vector<File_Class>& db_structure, int NumberOfRepresentationPerClass, int NumberOfClassifications, int InitWeightsMode)
{
	if (!InputVector.size())
	{
		cout << __func__ << "You need to Size VectorWeights before calling this function... Aborting!" << endl;
		exit(EXIT_FAILURE);
	}
	
	int z = 0, k = 0;
	
	VectorWeights.resize(NumberOfRepresentationPerClass);

	for (int i = 0; i < NumberOfRepresentationPerClass; i++)
	{
		VectorWeights[i].resize(NumberOfClassifications);
	
		for (int j = 0; j < NumberOfClassifications; j++)
			VectorWeights[i][j].resize(InputVector.size());
	}

	if (InitWeightsMode == 0)										//Init Randomly				
	{
		for (int i = 0; i < NumberOfRepresentationPerClass; i++)
			for (int j = 0; j < NumberOfClassifications; j++)
				for (int k = 0; k < VectorWeights[i][j].size(); k++)
					VectorWeights[i][j][k] = (float)(rand() % TOTALRANDOMQTY + (ADJUSTEDLOWERBOUND)) / (float)ULBOUNDPRECISION;
	}
	else if (InitWeightsMode == 1)									//Init from an input
	{
		for (int i = 0; i < NumberOfRepresentationPerClass; i++)
		{
			for (int j = 0; j < NumberOfClassifications; j++)
			{
					
				for (z; z < db_structure.size(); z++)			//Find Index containing an expected output
				{
					if (db_structure[z].ExpectedOutput == j)
						break;
				}

	
				for (int Sample = 0; Sample < db_structure[z].float_parsed_rawdata.size(); Sample++)
					for (int DataIndex = 0; DataIndex < NUMBEROFDATAPERSAMPLE; DataIndex++)
					{
						VectorWeights[i][j][k] = db_structure[z].float_parsed_rawdata[Sample][DataIndex];
						k++;
					}
				k = 0;
			}
		}
	}
	else
	{
		cout << __func__ << "You did not select a valid mode... Aborting!" << endl;
		exit(EXIT_FAILURE);
	}
}


void TYPE_LVQ::FindShortestDistance()
{
	ShortestDistanceID.ShortestDistance = 0;
	ShortestDistanceID.ClassNumber = 0;
	ShortestDistanceID.NumberOfrepresentation = 0;

	if (!VectorWeights.size())
	{
		cout << __func__ << "You need to Size VectorWeights before calling this function... Aborting!" << endl;
		exit(EXIT_FAILURE);
	}

	for (int i = 0; i < VectorWeights.size(); i++)
		for (int j = 0; j < VectorWeights[i].size(); j++)
		{
			CurrentDistance = 0;

			for (int k = 0; k < VectorWeights[i][j].size(); k++)
				CurrentDistance += pow(VectorWeights[i][j][k] - InputVector[k], 2);

			CurrentDistance = pow(CurrentDistance, 0.5);

			if (ShortestDistanceID.ShortestDistance > CurrentDistance)					//if we have new shortest distance, save it and it's position
			{
				ShortestDistanceID.NumberOfrepresentation = i;							//These values will be reset we correcting the weights
				ShortestDistanceID.ClassNumber			  = j;							//These values will be reset we correcting the weights
				ShortestDistanceID.ShortestDistance		  = CurrentDistance;			//These values will be reset we correcting the weights
			}
			else if (i==0 && j == 0)
			{
				ShortestDistanceID.NumberOfrepresentation = i;							//These values will be reset we correcting the weights
				ShortestDistanceID.ClassNumber = j;							//These values will be reset we correcting the weights
				ShortestDistanceID.ShortestDistance = CurrentDistance;			//These values will be reset we correcting the weights
			}
		}
}

float TYPE_LVQ::AdjustWeights()
{	
	float DeltaWeight;
	float BiggestChange = 0;
	float OldWeight;
	
	if (CurrentWantedOutput != ShortestDistanceID.ClassNumber)							//not same class then correct it
	{
		for (int k = 0; k < VectorWeights[ShortestDistanceID.NumberOfrepresentation][ShortestDistanceID.ClassNumber].size(); k++)
		{
						
			DeltaWeight = Alpha*(InputVector[k] - VectorWeights[ShortestDistanceID.NumberOfrepresentation][ShortestDistanceID.ClassNumber][k]);
			OldWeight = VectorWeights[ShortestDistanceID.NumberOfrepresentation][ShortestDistanceID.ClassNumber][k];
			VectorWeights[ShortestDistanceID.NumberOfrepresentation][ShortestDistanceID.ClassNumber][k] -= DeltaWeight;



			//cout << __func__ << ": Represention " << ShortestDistanceID.NumberOfrepresentation << " of Class C" << ShortestDistanceID.ClassNumber << " has been moved" << endl;
			//cout << __func__ << ": OldWeight=" << OldWeight << " ->  NewWeight=" << VectorWeights[ShortestDistanceID.NumberOfrepresentation][ShortestDistanceID.ClassNumber][k] << endl;
			//cout << __func__ << ": DeltaWeight=" << DeltaWeight << endl;
		}
		return DeltaWeight;
	}
	else
	{
		for (int k = 0; k < VectorWeights[ShortestDistanceID.NumberOfrepresentation][ShortestDistanceID.ClassNumber].size(); k++)
		{
			DeltaWeight = Alpha*(InputVector[k] - VectorWeights[ShortestDistanceID.NumberOfrepresentation][ShortestDistanceID.ClassNumber][k]);
			OldWeight = VectorWeights[ShortestDistanceID.NumberOfrepresentation][ShortestDistanceID.ClassNumber][k];
			VectorWeights[ShortestDistanceID.NumberOfrepresentation][ShortestDistanceID.ClassNumber][k] += DeltaWeight;

			//cout << __func__ << ": Represention " << ShortestDistanceID.NumberOfrepresentation << " of Class C" << ShortestDistanceID.ClassNumber << " has been moved" << endl;
			//cout << __func__ << ": OldWeight=" << OldWeight << " ->  NewWeight=" << VectorWeights[ShortestDistanceID.NumberOfrepresentation][ShortestDistanceID.ClassNumber][k] << endl;
			//cout << __func__ << ": DeltaWeight=" << DeltaWeight << endl;
		}
		return DeltaWeight;
	}
		
	return 0;
}


