/*-----------------------------Included Libraries----------------------------*/
#include <fstream>
#include <conio.h>
#include <iostream>
#include <vector>
#include <Windows.h>
#include <sstream>
#include "file_access.h"
#include "NeuralNet.h"
#include <string>
#include "lvq.h"

using namespace std;

/*-----------------------------Global Variables-----------------------------*/
double									LOWER_RANDOM_BOUND,
										DEFAULT_WEIGHT_VALUES,
										UPPER_RANDOM_BOUND;

float									LEARNING_VALUE_N,
										SMALLEST_THRESHOLD = .01,
										ALPHA ;

int										SUCCESS_RATE_THRESHOLD,
										NUMBER_OF_ITERATIONS,
										LOAD_SAVED_WEIGHTS,
										SIZE_OF_NETWORK,
										NUMBEROFDATAPERSAMPLE,
										TRAIN_A_NETWORK,
										ULBOUNDPRECISION,
										FIRST_LAYER_SIZE,
										NUMBEROFREPSPERCLASS ,
										NUMBEROFCLASSES ,
										ADJUSTEDLOWERBOUND,
										TOTALRANDOMQTY,
										INITWEIGHTSMODE;

std::string								TRAIN_DB_NAME,
										VC_DB_NAME,
										TEST_DB_NAME,
										CONFIG_PATH		 =   "LVQ_prep.config",
										WEIGHTS_BACKUP_PATH,
										LOAD_WEIGHTS_FROM_PATH,
										OUTPUT_LOGFILE;


INIT_WEIGHTS_FROM						DEFAULT_WEIGHT_MODE;


ACTIVATION_FUNCTION						ACTIVATIONFUNCTION;

ofstream								TrainingLVQFile;


constants::FILTERING_OPTION				FILTERING_OPTION_CHOSEN;


constants::NUMBER_OF_SAMPLES_ACCEPTED	NUMBER_OF_SAMPLE_PER_FILE;



int main(int argc, char *argv[])
{
	
	int									TrainCorrectPrediction = 0,
										VCCorrectPrediction = 0,
										TestCorrectPrediction = 0,
										count = 0,
										RandomFile = 0,
										ChangesHaveBeenMade,
										BiggestChange = SMALLEST_THRESHOLD + 1;


	std::vector<File_Class>				TrainingDataBase, 
										VCDataBase, 
										TestDataBase;

	TYPE_LVQ							LVQTrain, 
										LVQVC,	
										LVQTest;

	SavedFileSettings					MyFile;

	if (!DoesFileExist("LVQ_prep.config"))													//Check if config file present
	{
		MyFile.GenerateConfigFile();
		cout << "The File LVQ_prep.config cannot be found, we generated a new one for you" << endl;
	}

	ShowIntroductionAndPause();

	MyFile.ReadFromConfigFile();																//Fetch Parameters from config file
	MyFile.LoadGlobalVariablesWithMyConfiguartion();											//Load parameters to global vars


	if (LOAD_SAVED_WEIGHTS)
	{
		if (!DoesFileExist(TRAIN_DB_NAME + "SavedWeightsFor(" + OUTPUT_LOGFILE + ").bin"))
		{
			cout << "The file " << LOAD_WEIGHTS_FROM_PATH << ".bin doesn't exist, make sure you have the right file" << endl;
			cout << "And that you did not add the .bin extension to the parameter... Exiting..." << endl;
			cin.ignore();
			exit(EXIT_FAILURE);
		}

		LVQTrain.ReadCurrentLVQWeightsFromBin(TRAIN_DB_NAME + "SavedWeightsFor(" + OUTPUT_LOGFILE + ").bin",NUMBEROFREPSPERCLASS, NUMBEROFCLASSES, LVQTrain);
	}
	
	TOTALRANDOMQTY = (int)(abs(LOWER_RANDOM_BOUND*ULBOUNDPRECISION) + UPPER_RANDOM_BOUND*ULBOUNDPRECISION + 1);
	ADJUSTEDLOWERBOUND = (int)(LOWER_RANDOM_BOUND * ULBOUNDPRECISION);

	if (!DoesFileExist(TRAIN_DB_NAME + ".bin"))													//Check if we already DataBase in bin format
	{
		GenerateTXTtoBinFromDataBase(TRAIN_DB_NAME + ".bin", "Db\\txt_dist\\train\\",
			FILTERING_OPTION_CHOSEN,
			NUMBER_OF_SAMPLE_PER_FILE,
			TRAIN_DB_NAME + ".txt");
	}

	if (!DoesFileExist(VC_DB_NAME + ".bin"))													//Check if we already DataBase in bin format
	{
		GenerateTXTtoBinFromDataBase(VC_DB_NAME + ".bin", "Db\\txt_dist\\vc\\",
			FILTERING_OPTION_CHOSEN,
			NUMBER_OF_SAMPLE_PER_FILE,
			VC_DB_NAME + ".txt");
	}

	if (!DoesFileExist(TEST_DB_NAME + ".bin"))													//Check if we already DataBase in bin format
	{
		GenerateTXTtoBinFromDataBase(TEST_DB_NAME + ".bin", "Db\\txt_dist\\test\\",
			FILTERING_OPTION_CHOSEN,
			NUMBER_OF_SAMPLE_PER_FILE,
			TEST_DB_NAME + ".txt");
	}

	cout << "Fetching Data Set" << TRAIN_DB_NAME << " From Bin" << endl;
	ReadDbFromBin(TRAIN_DB_NAME + ".bin", TrainingDataBase);									//Load the DataBase From bin file
	cout << "Fetching Data Set" << VC_DB_NAME << " From Bin" << endl;
	ReadDbFromBin(VC_DB_NAME + ".bin", VCDataBase);												//Load the DataBase From bin file
	cout << "Fetching Data Set" << TEST_DB_NAME << " From Bin" << endl;
	ReadDbFromBin(TEST_DB_NAME + ".bin", TestDataBase);


	LVQTrain.InitializeVariables(ALPHA);
	LVQVC.InitializeVariables(ALPHA);
	LVQTest.InitializeVariables(ALPHA);

	LVQTrain.LoadInputsToVector(TrainingDataBase, 0);																//Needs to be called at least once
	LVQVC.LoadInputsToVector(VCDataBase, 0);
	LVQTest.LoadInputsToVector(TestDataBase, 0);


	if (!LOAD_SAVED_WEIGHTS) 
	{
		LVQTrain.InitWeights(TrainingDataBase, NUMBEROFREPSPERCLASS, NUMBEROFCLASSES, INITWEIGHTSMODE);				//Initialize the weights according to the mode
	}

	TrainingLVQFile.open(OUTPUT_LOGFILE + ".txt", std::ios::app);
	TrainingLVQFile << "_____________________________________________________________________" << endl;
	TrainingLVQFile << endl << endl << "Started Training Network with " << TRAIN_DB_NAME << endl;
	TrainingLVQFile << "Configuration: " << endl 
		<< "                     Number of elements/s: " << NUMBEROFDATAPERSAMPLE << endl
		<< "                     Alpha				 : " << ALPHA << endl
		<< "                     Rand LWR Value      : " << LOWER_RANDOM_BOUND << endl
		<< "                     Rand UPPER Value    : " << UPPER_RANDOM_BOUND << endl
		<< "                     Output Layer Size   : " << NUMBER_OF_OUTPUT << endl
		<< " (Check config file for details on params)" << endl << endl;

	TrainingLVQFile.close();

	for (int TrainingLoop = 0; TrainingLoop < NUMBER_OF_ITERATIONS; TrainingLoop++)									//Main Training Loop
	{
		for (int iterations =0;iterations < TrainingDataBase.size(); iterations++)									//Number of times to be done / loop
		{

			RandomFile = rand() % (TrainingDataBase.size() - 1);
			LVQTrain.LoadInputsToVector(TrainingDataBase, RandomFile);												//Charge InputVector to All LVQ's

			LVQTrain.FindShortestDistance();
			LVQTrain.AdjustWeights();


			if (LVQTrain.CurrentWantedOutput == LVQTrain.ShortestDistanceID.ClassNumber)
				TrainCorrectPrediction++;

		
		}
		LVQVC.VectorWeights = LVQTrain.VectorWeights;
		LVQTest.VectorWeights = LVQTrain.VectorWeights;
		for (int iterations = 0; iterations < VCDataBase.size(); iterations++)
		{
							//Charge InputVector to All LVQ's
			RandomFile = rand() % (VCDataBase.size() - 1);
			LVQVC.LoadInputsToVector(VCDataBase, RandomFile);								//Charge InputVector to All LVQ's

			LVQVC.FindShortestDistance();

			if (LVQVC.CurrentWantedOutput == LVQVC.ShortestDistanceID.ClassNumber)
				VCCorrectPrediction++;

		}
		for (int iterations = 0; iterations < TestDataBase.size(); iterations++)
		{

			RandomFile = rand() % (TestDataBase.size() - 1);
			LVQTest.LoadInputsToVector(TestDataBase, RandomFile);								//Charge InputVector to All LVQ's

										//Copy Weights fron training Network

			LVQTest.FindShortestDistance();

			if (LVQTest.CurrentWantedOutput == LVQTest.ShortestDistanceID.ClassNumber)
				TestCorrectPrediction++;

		}
		float TrainingPercentage = 100*(float)((float)TrainCorrectPrediction / (float)TrainingDataBase.size());
		float VCPercentage = 100*(float)((float)VCCorrectPrediction / (float)VCDataBase.size());
		float TestPercentage = 100*(float)((float)TestCorrectPrediction / (float)TestDataBase.size());

		cout << " TrainCorrectPrediction " <<  TrainingPercentage<< 
				" VCCorrectPrediction "    << 	VCPercentage	<<
			    " TestCorrectPrediction "  << TestPercentage << endl;
		
		TrainingLVQFile.open(OUTPUT_LOGFILE + ".txt", std::ios::app);
		TrainingLVQFile << "CLASS(" << NUMBEROFCLASSES << ") REP(" << NUMBEROFREPSPERCLASS << ") Training Iteration: " << TrainingLoop
			<< " Learning Success Rate= " << TrainingPercentage << " %"
			<< " Cross Validation Success Rate= " << VCPercentage << " %"
			<< " Test Success Rate= " << TestPercentage << " %" << endl;
		TrainingLVQFile.close();

		if (VCPercentage > SUCCESS_RATE_THRESHOLD)									//Check if we've over learned and bails out, saving the weights
		{
			LVQTrain.WriteCurrentLVQWeightsToBin(TRAIN_DB_NAME + "SavedWeightsFor(" + OUTPUT_LOGFILE + ").bin", NUMBEROFREPSPERCLASS, NUMBEROFCLASSES,LVQTrain);
			exit(EXIT_SUCCESS);
		}

		TrainCorrectPrediction	= 0;
		VCCorrectPrediction		= 0;
		TestCorrectPrediction	= 0;

		if (kbhit())
		{
			char ch = getch();
			if(ch=='q' || ch=='Q')
			{
				LVQTrain.WriteCurrentLVQWeightsToBin(TRAIN_DB_NAME + "SavedWeightsFor(" + OUTPUT_LOGFILE + ").bin", NUMBEROFREPSPERCLASS, NUMBEROFCLASSES, LVQTrain);
				exit(EXIT_SUCCESS);
			}
		}
	}

	return 0;
}