_____________________________________________________________________


Started Training Network with DataBase_train_B_40
Configuration: 
                     Activation Function : 2
                     Weight Values Mode  : 1
                     Default Values		 : 0.1
                     Network Size        : 3
                     Number of elements/s: 12
                     Learning Value (N)  : 0.1
                     Rand LWR Value      : -0.1
                     Rand UPPER Value    : 0.1
                     Output Layer Size   : 0.1
 (Check config file for details on params)

FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 0 Learning Success Rate= 14.1045 % Cross Validation Success Rate= 19.1667 % Test Success Rate= 19.6154 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 1 Learning Success Rate= 29.7015 % Cross Validation Success Rate= 38.3333 % Test Success Rate= 35.3846 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 2 Learning Success Rate= 46.7164 % Cross Validation Success Rate= 62.5 % Test Success Rate= 61.2821 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 3 Learning Success Rate= 60.0746 % Cross Validation Success Rate= 57.5 % Test Success Rate= 60 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 4 Learning Success Rate= 69.3284 % Cross Validation Success Rate= 66.6667 % Test Success Rate= 67.1795 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 5 Learning Success Rate= 73.9552 % Cross Validation Success Rate= 67.5 % Test Success Rate= 66.7949 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 6 Learning Success Rate= 78.5075 % Cross Validation Success Rate= 61.6667 % Test Success Rate= 67.5641 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 7 Learning Success Rate= 81.2687 % Cross Validation Success Rate= 74.1667 % Test Success Rate= 71.2821 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 8 Learning Success Rate= 86.194 % Cross Validation Success Rate= 65.8333 % Test Success Rate= 70.7692 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 9 Learning Success Rate= 88.0597 % Cross Validation Success Rate= 72.5 % Test Success Rate= 72.6923 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 10 Learning Success Rate= 90.6716 % Cross Validation Success Rate= 74.1667 % Test Success Rate= 72.8205 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 11 Learning Success Rate= 92.0149 % Cross Validation Success Rate= 71.6667 % Test Success Rate= 75.1282 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 12 Learning Success Rate= 90.597 % Cross Validation Success Rate= 75.8333 % Test Success Rate= 75.2564 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 13 Learning Success Rate= 93.7313 % Cross Validation Success Rate= 76.6667 % Test Success Rate= 76.4103 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 14 Learning Success Rate= 94.2537 % Cross Validation Success Rate= 77.5 % Test Success Rate= 77.1795 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 15 Learning Success Rate= 95.4478 % Cross Validation Success Rate= 69.1667 % Test Success Rate= 73.3333 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 16 Learning Success Rate= 95.0746 % Cross Validation Success Rate= 74.1667 % Test Success Rate= 73.0769 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 17 Learning Success Rate= 96.1194 % Cross Validation Success Rate= 74.1667 % Test Success Rate= 76.2821 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 18 Learning Success Rate= 96.8657 % Cross Validation Success Rate= 75 % Test Success Rate= 76.4103 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 19 Learning Success Rate= 97.1642 % Cross Validation Success Rate= 80 % Test Success Rate= 74.2308 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 20 Learning Success Rate= 98.3582 % Cross Validation Success Rate= 75.8333 % Test Success Rate= 77.4359 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 21 Learning Success Rate= 97.4627 % Cross Validation Success Rate= 75.8333 % Test Success Rate= 75.5128 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 22 Learning Success Rate= 97.6119 % Cross Validation Success Rate= 80 % Test Success Rate= 77.1795 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 23 Learning Success Rate= 98.7313 % Cross Validation Success Rate= 76.6667 % Test Success Rate= 75.5128 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 24 Learning Success Rate= 98.0597 % Cross Validation Success Rate= 79.1667 % Test Success Rate= 78.4615 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 25 Learning Success Rate= 98.8806 % Cross Validation Success Rate= 76.6667 % Test Success Rate= 76.0256 %
FirstLayer 100 Neurons HiddenLayer 55 Neurons Training Iteration 26 Learning Success Rate= 99.5522 % Cross Validation Success Rate= 75.8333 % Test Success Rate= 77.3077 %
